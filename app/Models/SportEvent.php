<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class SportEvent extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sport_events';

    protected $fillable = [
        'team1_id',
        'team2_id',
        'time',
        'live',
        'result1',
        'result2',
        'tournament_id',
    ];

    public static $rules = [
        'team1_id' => 'required',
        'team2_id' => 'required',
        'time' => 'required',
    ];

    public static $statuses = [
        self::STATUS_IWILLGO => 'Пойду',
        self::STATUS_IHERE => 'Я здесь'
    ];

    const STATUS_IWILLGO = 1;
    const STATUS_IHERE = 2;

    public function bars()
    {
        return $this->belongsToMany('App\Models\Bar', 'bar_events', 'event_id', 'bar_id');
    }

    public function tournament()
    {
        return $this->hasOne('App\Models\CatalogItem', 'id', 'tournament_id');
    }

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    /**
     * Get the team1 record associated with the teams.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function team1()
    {
        return $this->hasOne('App\Models\Team', 'id', 'team1_id');
    }

    /**
     * Get the team2 record associated with the teams.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function team2()
    {
        return $this->hasOne('App\Models\Team', 'id', 'team2_id');
    }

    public function save(array $options = array())
    {
        if (Input::get('time')) {
            $this->time = strtotime(Input::get('time'));
        }

        parent::save($options);
    }

    public function getDescr()
    {
        return $this->team1->name . ' VS ' . $this->team2->name . ' ' . date('d.m.Y H:i', $this->time) . ' ' . ($this->live ? 'Live':'');
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'team1' => $this->team1()->getResults(),
            'team2' => $this->team2()->getResults(),
            'time' => $this->time,
            'live' => $this->live,
            'result1' => $this->result1,
            'result2' => $this->result2,
            'tournament' => $this->tournament()->getResults(),
            'iwillgo' => isset($this->iwillgo),
        ];
    }
}
