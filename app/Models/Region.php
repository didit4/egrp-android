<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Region extends Model
{

    public function placeType()
    {
        return $this->hasOne('App\Models\PlaceType', 'place_type_name_id', 'place_type_name_id');
    }

    public function getFullName()
    {
        if ($this->place_type_name_id == 3) {
            return $this->name;
        }
        return $this->name . ' ' . $this->placeType->full_name;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => (string) $this->place_id,
            'name' => $this->getFullName()
        ];
    }
}
