<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use DB;
use Config;
use Mail;
use Carbon\Carbon;

class Req extends Model
{

    protected $table = 'reqs';

    protected $fillable = [
        'status',
        'cadastral_num',
        'conditional_num',
        'srr_num',
        'limit_num',
        'name',
        'phone',
        'email',
        'operator_id',
        'region_id',
        'region_name',
        'area_id',
        'area_name',
        'locality_id',
        'locality_name',
        'street_id',
        'street_name',
        'house',
        'price',
        'street_type',
        'street_type_name',
        'first_name',
        'first_phone',
        'first_email',
        'first_region_id',
        'first_region_name',
        'first_area_id',
        'first_area_name',
        'first_locality_id',
        'first_locality_name',
        'first_street_id',
        'first_street_name',
        'first_house',
        'first_cadastral_num',
        'first_conditional_num',
        'first_srr_num',
        'first_limit_num',
        'first_street_type',
        'first_street_type_name',
        'file',
        'processed_at',
        'ros_at',
        'xml_at',
        'sent_at',
        'watched_at',
        'user_watch_id',
        'pay_type',
        'type',
        'room',
        'building',
        'housing',
        'first_room',
        'first_building',
        'first_housing',
        'not_at',
        'address',
        'commission',
        'notpayed'
    ];

    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required|email',
        'region_id' => 'required',
        'region_name' => 'required',
    ];

    public static $rulesNums = [
        'cadastral_num' => 'required_without_all:conditional_num,srr_num,limit_num',
        'conditional_num' => 'required_without_all:cadastral_num,srr_num,limit_num',
        'srr_num' => 'required_without_all:conditional_num,cadastral_num,limit_num',
        'limit_num' => 'required_without_all:conditional_num,srr_num,cadastral_num',
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required|email',
    ];

    public static $statuses = [
        'Новая',
        'Обрабатывается',
        'В Росреестре',
        'Получен XML',
        'Отправлено на email',
        'Нет в Росреестре'
    ];

    public static $statusesColors = [
        'label-danger',
        'label-warning',
        'label-primary',
        'label-info',
        'label-success'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function payType()
    {
        return $this->hasOne('App\Models\PayType', 'type', 'pay_type');
    }

    public function reqType()
    {
        return $this->hasOne('App\Models\ReqType', 'id', 'type');
    }

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    public static function emulateDataNewReq()
    {
        $attributes = [
            'cadastral_num' => self::getENum(),
            'name' => self::getEName(),
            'phone' => self::getEPhone(),
            'email' => 'maria@gmail.com',
            'region_id' => '1',
            'region_name' => 'Московская обл.',
            'area_id' => '1',
            'area_name' => 'Москва',
            'locality_id' => '1',
            'locality_name' => self::getELocality(),
            'street_id' => '1',
            'street_name' => 'Ленина',
            'house' => '1',
            'price' => '250'
        ];

        return $attributes;
    }

    public static function getEName()
    {
        $names = [
            'Мария',
            'Вера',
            'Надежда',
            'Любовь',
            'Иван',
            'Петр',
            'Михаил',
            'Василий',
        ];

        return $names[mt_rand(0, count($names) - 1)];
    }

    public static function getELocality()
    {
        $cities = [
            'Москва',
            'Санкт-Петербург',
            'Екатеринбург',
            'Новосибирск',
            'Волгоград',
            'Владивосток',
            'Нижний Тагил',
            'Калининград',
        ];

        return $cities[mt_rand(0, count($cities) - 1)];
    }

    public static function getENum()
    {
        return mt_rand(10,99) . ':' . mt_rand(10,99) . ':' . mt_rand(1000000,9999999) . ':' . mt_rand(100,999);
    }

    public static function getEPhone()
    {
        return '89' . mt_rand(111111111, 999999999);
    }

    public static function fillFristFields($data)
    {
        $firstFields = [
            'first_name' => isset($data['name']) ? $data['name'] : '',
            'first_phone' => isset($data['phone']) ? $data['phone'] : '',
            'first_email' => isset($data['email']) ? $data['email'] : '',
            'first_region_id' => isset($data['region_id']) ? $data['region_id'] : '',
            'first_region_name' => isset($data['region_name']) ? $data['region_name'] : '',
            'first_area_id' => isset($data['area_id']) ? $data['area_id'] : '',
            'first_area_name' => isset($data['area_name']) ? $data['area_name'] : '',
            'first_locality_id' => isset($data['locality_id']) ? $data['locality_id'] : '',
            'first_locality_name' => isset($data['locality_name']) ? $data['locality_name'] : '',
            'first_street_id' => isset($data['street_id']) ? $data['street_id'] : '',
            'first_street_name' => isset($data['street_name']) ? $data['street_name'] : '',
            'first_street_type' => isset($data['street_type']) ? $data['street_type'] : '',
            'first_street_type_name' => isset($data['street_type_name']) ? $data['street_type_name'] : '',
            'first_house' => isset($data['house']) ? $data['house'] : '',
            'first_cadastral_num' => isset($data['cadastral_num']) ? $data['cadastral_num'] : '',
            'first_conditional_num' => isset($data['conditional_num']) ? $data['conditional_num'] : '',
            'first_srr_num' => isset($data['srr_num']) ? $data['srr_num'] : '',
            'first_limit_num' => isset($data['limit_num']) ? $data['limit_num'] : '',
            'first_room' => isset($data['room']) ? $data['room'] : '',
            'first_building' => isset($data['building']) ? $data['building'] : '',
            'first_housing' => isset($data['housing']) ? $data['housing'] : '',
        ];

        return array_merge($data, $firstFields);
    }

    public function getNum()
    {
        if ($this->cadastral_num) {
            return $this->cadastral_num;
        }
        if ($this->conditional_num) {
            return $this->conditional_num;
        }
        if ($this->srr_num) {
            return $this->srr_num;
        }
        if ($this->limit_num) {
            return $this->limit_num;
        }
    }

    public function getNumName()
    {
        if ($this->cadastral_num) {
            return 'Кадастровый номер';
        }
        if ($this->conditional_num) {
            return 'Условный номер';
        }
        if ($this->srr_num) {
            return 'Номер государственной регистрации права';
        }
        if ($this->limit_num) {
            return 'Номер ограничения (обременения) права';
        }
    }

    public function getFirstNum()
    {
        if ($this->cadastral_num) {
            return $this->cadastral_num;
        }
        if ($this->conditional_num) {
            return $this->conditional_num;
        }
        if ($this->srr_num) {
            return $this->srr_num;
        }
        if ($this->limit_num) {
            return $this->limit_num;
        }
    }

    public function getFirstNumName()
    {
        if ($this->cadastral_num) {
            return 'Кадастровый номер';
        }
        if ($this->conditional_num) {
            return 'Условный номер';
        }
        if ($this->srr_num) {
            return 'Номер государственной регистрации права';
        }
        if ($this->limit_num) {
            return 'Номер ограничения (обременения) права';
        }
    }

    public static function getSumTodayRequest()
    {
        return self::getSumDayRequest();
    }

    /**
     * date("Y-m-d", strtotime("2016-07-01"))
     *
     * @param string $day
     * */
    public static function getSumDayRequest($day = '')
    {
        if (!$day) {
            $day = date("Y-m-d");
        }
        $date = new \DateTime($day);
        $result = DB::table('reqs')->select(
                DB::raw('((SUM(price) - SUM(price) * commission / 100) - (SUM(price) - SUM(price) * commission / 100) * 0.13) as r_income')
            )
            ->whereDate('created_at', '=', $date->format('Y-m-d'))
            ->first();
        return $result->r_income;
    }

    public static function getCountTodayRequest()
    {
        return self::getCountDayRequest();
    }

    /**
     * date("Y-m-d", strtotime("2016-07-01"))
     *
     * @param string $day
     * */
    public static function getCountDayRequest($day = '')
    {
        if (!$day) {
            $day = date("Y-m-d");
        }
        $date = new \DateTime($day);
        return self::whereDate('created_at', '=', $date->format('Y-m-d'))->count();
    }

    public static function getStatistic()
    {
        $result = DB::table('reqs')->select(
                DB::raw('DATE(created_at) as r_date'),
                DB::raw('COUNT(*) as r_count'),
                DB::raw('SUM(price) as r_sum'),
                DB::raw('(SUM(price) * commission / 100) as r_commission'),
                DB::raw('((SUM(price) - SUM(price) * commission / 100) * 0.13) as r_tax'),
                DB::raw('((SUM(price) - SUM(price) * commission / 100) - (SUM(price) - SUM(price) * commission / 100) * 0.13) as r_income')
            )
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy(DB::raw('DATE(created_at)'), 'desc')
            ->paginate(10);

        return $result;
    }

    public static function getStatisticForExcel($monthStart = '', $monthEnd = '')
    {
        $result = DB::table('reqs')->select(
                DB::raw('DATE(created_at) as r_date'),
                DB::raw('COUNT(*) as r_count'),
                DB::raw('SUM(price) as r_sum'),
                DB::raw('(SUM(price) * commission / 100) as r_commission'),
                DB::raw('((SUM(price) - SUM(price) * commission / 100) * 0.13) as r_tax'),
                DB::raw('((SUM(price) - SUM(price) * commission / 100) - (SUM(price) - SUM(price) * commission / 100) * 0.13) as r_income')
            );
        if ($monthStart) {
            $result = $result->where('created_at', '>=', $monthStart)
                ->where('created_at', '<=', $monthEnd);
        }
        $result = $result->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy(DB::raw('DATE(created_at)'), 'desc')
            ->get();
        $resultArr = [['Дата', 'Кол-во новых', 'Сумма, руб', 'Сумма комиссии, руб',  'Налоги 13%, руб', 'Прибыль, руб']];
        foreach($result as $item) {
            $resultArr[] = [
                $item->r_date,
                $item->r_count,
                $item->r_sum,
                $item->r_commission,
                $item->r_tax,
                $item->r_income
            ];
        }
        return $resultArr;
    }

    public static function getStatisticSumCurrMonth()
    {
        return self::getStatisticSumMonth();
    }

    public static function getStatisticSumMonth($monthStart = '', $monthEnd = '')
    {
        if (!$monthStart) {
            $monthStart = Carbon::now()->startOfMonth();
            $monthEnd = Carbon::now()->endOfMonth();
        }
        $result = DB::table('reqs')->select(
            DB::raw('((SUM(price) - SUM(price) * commission / 100) - (SUM(price) - SUM(price) * commission / 100) * 0.13) as r_income')
            )
            ->where('created_at', '>=', $monthStart)
            ->where('created_at', '<=', $monthEnd)
            ->first();
        return $result->r_income ? :0;
    }

    public function save(array $options = array())
    {
        if (Input::hasFile('file')) {

            $file = Input::file('file');
            $fpath = base_path() . '/public/static/xml/';

            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fname = sha1($filename . time()) . '.' . $extension;

            $this->file = '/xml/' . $fname;

            $file->move($fpath, $fname);
        }
        if (Input::hasFile('file2')) {

            $file = Input::file('file2');
            $fpath = base_path() . '/public/static/pdf/';
            if($file->getSize() / 1048576 > 10) {
                session(['fileBig' => true]);
                return false;
            }
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $fname = sha1($filename . time()) . '.' . $extension;

            $this->file2 = '/pdf/' . $fname;

            $file->move($fpath, $fname);
        }

        parent::save($options);
    }

    public function getFilePath()
    {

        return Config::get('app.url') . $this->file;
    }
    public function getFile2Path()
    {

        return Config::get('app.url') . $this->file2;
    }

    public function getStatusName()
    {
        return isset(self::$statuses[$this->status]) ? self::$statuses[$this->status] : 'Новая';
    }

    public function getStatusClassColor()
    {
        return isset(self::$statusesColors[$this->status]) ? self::$statusesColors[$this->status] : 'label-danger';
    }

    public function getCreatedAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d.m.Y, H:i');
    }

    public function getUpdatedAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at)->format('d.m.Y, H:i');
    }

    public function getProcessedAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->processed_at)->format('d.m.Y, H:i');
    }

    public function getRosAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->ros_at)->format('d.m.Y, H:i');
    }

    public function getXmlAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->xml_at)->format('d.m.Y, H:i');
    }

    public function getSentAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->sent_at)->format('d.m.Y, H:i');
    }

    public function getNotAt()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->not_at)->format('d.m.Y, H:i');
    }

    public function isBusy()
    {
        return $this->user_watch_id && $this->user_watch_id != \Auth::user()->id && $this->watched_at > Carbon::now()->subMinute();
    }

    public function sendEmail()
    {
        $model = $this;
        Mail::send('emails.request', [], function ($message) use($model){

            $message->from('hello@egrp.today', 'egrp.today');
            $message->to($model->email)->subject('Результат вашей заявки на egrp.today');
            if ($model->file) {
                $message->attach($model->getFilePath());
            }
            if ($model->file2) {
                $message->attach($model->getFile2Path());
            }
        });
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = [
            'id' => (string) $this->id,
            'phone' => (string) $this->phone,
            'status' => (string) $this->status,
            'created' => $this->getCreatedAt(),
//            'date' => $this->getUpdatedAt()
        ];
        if ($this->getProcessedAt() != '30.11.-0001, 00:00') {
            $data['processed_at'] = $this->getProcessedAt();
        }
        if ($this->getRosAt() != '30.11.-0001, 00:00') {
            $data['ros_at'] = $this->getRosAt();
        }
        if ($this->getXmlAt() != '30.11.-0001, 00:00') {
            $data['xml_at'] = $this->getXmlAt();
        }
        if ($this->getSentAt() != '30.11.-0001, 00:00') {
            $data['sent_at'] = $this->getSentAt();
        }
        if ($this->getNotAt() != '30.11.-0001, 00:00') {
            $data['not_at'] = $this->getNotAt();
        }
        return $data;
    }
}
