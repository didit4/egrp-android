<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Bonus extends Model
{

    protected $table = 'bonus';

    protected $fillable = [
        'user_id',
        'event_id',
        'bar_id',
        'to_time',
        'active',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function event()
    {
        return $this->hasOne('App\Models\SportEvent', 'id', 'event_id');
    }

    public function bar()
    {
        return $this->hasOne('App\Models\Bar', 'id', 'bar_id');
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'to_time' => $this->to_time,
            'active' => $this->active,
            'created_at' => $this->created_at,
        ];
    }
}
