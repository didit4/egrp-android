<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class PayType extends Model
{
    protected $table = 'pay_types';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'percent',
        'type',
    ];

    public static $rules = [
        'name' => 'required',
        'percent' => 'required',
        'type' => 'required',
    ];
}
