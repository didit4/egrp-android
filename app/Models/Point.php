<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;

class Point extends Model
{

    protected $fillable = [
        'val',
        'descr',
        'user_id',
    ];

    public static $rules = array(
        'val' => 'required',
        'descr' => 'required',
    );

    public static function rules($id = null)
    {
        $rules = self::$rules;

        if ($id) {
            foreach ($rules as &$rule) {
                $rule = str_replace(':id', $id, $rule);
            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'val' => $this->val,
            'descr' => $this->descr,
        ];
    }
}
