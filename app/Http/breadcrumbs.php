<?php

Breadcrumbs::register('admin', function($breadcrumbs)
{
    $breadcrumbs->push('Admin', '/admin/');
});

Breadcrumbs::register('reqs', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Список заявок' , route('admin.reqs.index'));
});

Breadcrumbs::register('reqs_show', function($breadcrumbs, $req)
{
    $breadcrumbs->parent('reqs');
    $breadcrumbs->push('Заявка # ' . $req->id, route('admin.reqs.show', $req->id));
});

Breadcrumbs::register('reqs_show_first', function($breadcrumbs, $req)
{
    $breadcrumbs->parent('reqs_show', $req);
    $breadcrumbs->push('Первоначальная версия');
});
Breadcrumbs::register('reqs_edit', function($breadcrumbs, $req)
{
    $breadcrumbs->parent('reqs');
    $breadcrumbs->push('Редактировать заявку # ' . $req->id, route('admin.reqs.edit', $req->id));
});
Breadcrumbs::register('reqs_statistic', function($breadcrumbs)
{
    $breadcrumbs->parent('reqs');
    $breadcrumbs->push('Статистика заявок');
});
