<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Bar;
use App\Models\Catalog;
use App\Models\SportEvent;
use Illuminate\Support\Facades\Response;
use Validator;

class BarController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $bars = Bar::all();
        return view('admin.bars.index', compact('bars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $events = SportEvent::all();
        $metro = Catalog::where('name', 'Метро')->first();

        $stations = [];
        if ($metro) {
            $stations = $metro->getListForDropdown('Выберите метро');
        }
        $weeksDays = Bar::$weeksDays;

        return view('admin.bars.create', compact('events', 'stations', 'weeksDays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Bar::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.bars.create')
                ->withErrors($validator)
                ->withInput();
        }

        Bar::create($input);

        return redirect()->route('admin.bars.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = Bar::find($id);
        return view('admin.bars.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Bar::find($id);
        if($model === null){
            return redirect('admin/bars');
        }
        $events = SportEvent::where('active', true)->get();
        $arrModelEvents = $model->sportEvents;
        $arrModelEventsIds = [];
        foreach ($arrModelEvents as $event) {
            $arrModelEventsIds[] = $event->id;
        }
        $metro = Catalog::where('name', 'Метро')->first();
        $stations = [];
        if ($metro) {
            $stations = $metro->getListForDropdown('Выберите метро');
        }
        $offers = $model->offers;

        $infoItems = Catalog::where('name', 'Услуги и удобства в барах')->first()->items()->getResults();
        $arrInfoItemsIds = [];
        $arrInfoItems = $model->info();
        foreach ($arrInfoItems as $infoItem) {
            if ($infoItem->active) {
                $arrInfoItemsIds[] = $infoItem->id;
            }
        }

        $weeksDays = Bar::$weeksDays;
        $schedule = $model->getSchedule();
        return view('admin.bars.edit', compact('model', 'events', 'arrModelEventsIds', 'stations', 'offers', 'infoItems', 'arrInfoItemsIds', 'weeksDays', 'schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $validator = Validator::make($input, Bar::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.bars.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Bar::find($id);
        $model->update($input);
        return redirect('admin/bars');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Bar::find($id)->delete();
        return response()->json();
    }

    public function switch_active($id)
    {
        $bar = Bar::find($id);
        if($bar === null){
            return redirect('admin/bars');
        }
        $bar->active = !$bar->active;
        $bar->save();
        return redirect('admin/bars');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function event($id)
    {
        $event = SportEvent::find($id);
        $bars = $event->bars()->getResults();
        return view('admin.bars.index', compact('bars'));
    }

}
