<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Subscriber;
use Illuminate\Support\Facades\Response;

class SubscribeController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $subscrs = Subscriber::all();
        return view('admin.subscr.index', compact('subscrs'));
    }

}
