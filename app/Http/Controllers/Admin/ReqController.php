<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\PayType;
use App\Models\Place;
use App\Models\Region;
use App\Models\Req;
use App\Models\Setting;
use App\Models\StType;
use Illuminate\Support\Facades\Response;
use Validator;
use Carbon\Carbon;
use Mail;
use Input;

class ReqController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $reqs = Req::orderBy('id', 'desc');
        $input = $this->request->all();
        $params = [];
        $isSearch = false;
        if ($input) {
            if(isset($input['num']) && $input['num']) {
                $params['num'] = $input['num'];
                $reqs = $reqs->where(function($query) use ($input)
                {
                    $query->orWhere('cadastral_num', 'like', '%'.$input['num'].'%')
                        ->orWhere('conditional_num', 'like', '%'.$input['num'].'%')
                        ->orWhere('srr_num', 'like', '%'.$input['num'].'%')
                        ->orWhere('limit_num', 'like', '%'.$input['num'].'%');
                });
            }
            if(isset($input['loc']) && $input['loc']) {
                $params['loc'] = $input['loc'];
                $reqs = $reqs->where('locality_name', 'like', '%'.$input['loc'].'%');
            }
            if(isset($input['name']) && $input['name']) {
                $params['name'] = $input['name'];
                $reqs = $reqs->where('name', 'like', '%'.$input['name'].'%');
            }
            if(isset($input['phone']) && $input['phone']) {
                $params['phone'] = $input['phone'];
                $reqs = $reqs->where('phone', 'like', '%'.$input['phone'].'%');
            }
            $isSearch = true;
        }
        $reqs = $reqs->paginate(10);
        if ($this->request->ajax()) {
            return response()->json(['success' => true, 'requests' => $reqs->toJson() ]);
        }
        $price = Setting::find(1);
        $sumTodayRequest = Req::getSumTodayRequest();
        $countTodayRequest = Req::getCountTodayRequest();
        $message = '';
        if (!count($reqs)) {
            $message = 'По вашему запросу ничего не найдено';
        }
        return view('admin.reqs.index', compact('reqs', 'price', 'sumTodayRequest', 'countTodayRequest', 'params', 'message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.reqs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();
        $isEmulate = false;
        if(isset($input['isEmulate']) && $input['isEmulate']) {
            $input = Req::emulateDataNewReq();
            $isEmulate = true;
        }
        $input = Req::fillFristFields($input);

        $validator = Validator::make($input, Req::$rules);

        if ($validator->fails()) {
            if($isEmulate) {
                return response()->json(['success' => false, 'errors' => 'Ошибка валидации']);
            }
            return redirect()->route('admin.reqs.create')
                ->withErrors($validator)
                ->withInput();
        }

        Req::create($input);

        if($isEmulate) {
            return response()->json(['success' => true]);
        }
        return redirect()->route('admin.reqs.index');
    }

    public function switchActive($id)
    {
        $faq = Faq::find($id);
        if ($faq) {
            $faq->active = !$faq->active;
            $faq->save();
        }
        return redirect()->route('admin.reqs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = Req::find($id);
        if ($model->isBusy()) {
            return redirect()->route('admin.reqs.index');
        }
        $region = null;
        if ($model->cadastral_num && isset(explode(':', $model->cadastral_num)[0])) {
            $regionNum = explode(':', $model->cadastral_num)[0];
            $region = Region::where('num', $regionNum)->first();
        }
        $model->user_watch_id = \Auth::user()->id;
        $model->watched_at = Carbon::now()->addMinute();
        $model->save();
        $statuses = Req::$statuses;
        $message = session('fileBig') ? 'Файл слишком большой': '';
        $this->request->session()->forget('fileBig');
        return view('admin.reqs.show', compact('model', 'statuses', 'message', 'region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Req::find($id);
        if ($model->isBusy()) {
            return redirect()->route('admin.reqs.index');
        }
        $model->user_watch_id = \Auth::user()->id;
        $model->watched_at = Carbon::now()->addMinute();
        $model->save();
        $statuses = Req::$statuses;
        $colors = Req::$statusesColors;

        $regions = Region::orderBy('sort', 'DESC')->orderBy('name', 'ASC')->get();

        if ($model->region_id) {
            $areas = Place::where('region_place_id', $model->region_id)
                ->where('hide', 0)
                ->where(function ($query) {
                    $query->where('type', 2)
                        ->orWhere('type', 3);
                })
                ->orderBy('name', 'ASC')->get();
        }
        if ($model->area_id) {
            $places = Place::where('parent_place_id', $model->area_id)
                ->where('hide', 0)
                ->orderBy('name', 'ASC')->get();
        }
        $sTypes = StType::orderBy('name', 'ASC')->get();
        return view('admin.reqs.edit', compact('model', 'statuses', 'colors', 'regions', 'areas', 'places', 'sTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $validator = Validator::make($input, Req::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.reqs.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Req::find($id);
        $model->update($input);
        return redirect()->route('admin.reqs.show', [$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Bar::find($id)->delete();
        return response()->json();
    }

    public function statistic()
    {
        if (!\Auth::getUser()->isAdmin()) {
            return redirect()->route('admin.reqs.index');
        }
        $statistic = Req::getStatistic();
        $sumCurrMonth = Req::getStatisticSumCurrMonth();
        $months = ["1"=>"Январь","2"=>"Февраль","3"=>"Март","4"=>"Апрель","5"=>"Май", "6"=>"Июнь", "7"=>"Июль","8"=>"Август","9"=>"Сентябрь","10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь"];
        $currMonth = date('n');
        return view('admin.reqs.statistic', compact('statistic', 'sumCurrMonth', 'currMonth', 'months'));
    }

    public function first($id)
    {
        $model = Req::find($id);
        return view('admin.reqs.first', compact('model'));
    }


    public function change_month()
    {
        $input = $this->request->all();
        $sumCurrMonth = Req::getStatisticSumMonth(
            Carbon::create(2016, $input['month'], 1, 12, 0, 0)->startOfMonth(),
            Carbon::create(2016, $input['month'], 1, 12, 0, 0)->endOfMonth()
        );

        return response()->json(['sumCurrMonth' => $sumCurrMonth]);
    }

    public function excel()
    {

        \Excel::create('Filename', function($excel){

            $excel->sheet('Sheetname', function($sheet) {
                $input = $this->request->all();
                if (isset($input['month'])) {
                    $statistic = Req::getStatisticForExcel(
                        Carbon::create(2016, $input['month'], 1, 12, 0, 0)->startOfMonth(),
                        Carbon::create(2016, $input['month'], 1, 12, 0, 0)->endOfMonth()
                    );
                } else {
                    $statistic = Req::getStatisticForExcel();
                }
                $sheet->fromArray($statistic);

            });

        })->export('xls');
    }

    public function switch_status($id, $status)
    {
        $req = Req::find($id);
        if ($req && $status) {
            if ($status == 4) {
                $req->sendEmail();
            }
            $req->status = $status;
            $req->user_id = \Auth::user()->id;
            switch($status) {
                case 1:
                    $req->processed_at = Carbon::now();
                    break;
                case 2:
                    $req->ros_at = Carbon::now();
                    break;
                case 3:
                    $req->xml_at = Carbon::now();
                    break;
                case 4:
                    $req->sent_at = Carbon::now();
                    break;
                case 5:
                    $req->not_at = Carbon::now();
                    break;
            }
            $req->save();
        }
        return redirect()->route('admin.reqs.show', $id);
    }

    /**
     * @param $id
     * @return Response
     */
    public function load_file($id)
    {
        $input = $this->request->all();
        $model = Req::find($id);
        if ($model->status == 2) {
            $model->status = 3;
        }
        $model->xml_at = Carbon::now();
        $model->update($input);

        if ($model->status == 4) {
            $model->sendEmail();
        }

        return redirect()->route('admin.reqs.show', $id);
    }

    /**
     * @param $id
     * @return Response
     */
    public function update_watch($id)
    {
        $model = Req::find($id);
        $model->user_watch_id = \Auth::user()->id;
        $model->watched_at = Carbon::now()->addMinute();
        $model->save();

        return response()->json(['success' => true]);
    }

    public function switchPayed($id)
    {
        $model = Req::find($id);
        if ($model) {
            $model->notpayed = !$model->notpayed;
            $model->save();
        }
        return response()->json(['success' => true]);
    }

}
