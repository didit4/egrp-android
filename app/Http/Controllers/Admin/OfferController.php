<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Bar;
use App\Models\Offer;
use Illuminate\Support\Facades\Response;
use Validator;

class OfferController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $offers = Offer::all();
        return view('admin.bars.index', compact('offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Bar $bar
     * @return Response
     */
    public function create($bar)
    {
        return view('admin.offers.create', compact('bar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Bar $bar
     * @return Response
     */
    public function store($bar)
    {
        $bar = Bar::find($bar);
        if ($bar) {
            $input = $this->request->all();

            $validator = Validator::make($input, Offer::$rules);
            if ($validator->fails()) {
                return redirect()->route('admin.bars.offers.create', [$bar->id])
                    ->withErrors($validator)
                    ->withInput();
            }
            $item = Offer::create($input);
            $bar->offers()->save($item);
        }

        return redirect()->route('admin.bars.edit', [$bar->id, '#offers']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Bar $bar
     * @param  int $id
     * @return Response
     */
    public function edit($bar, $id)
    {
        $bar = Bar::find($bar);
        if ($bar) {
            $model = Offer::find($id);
            if ($model === null) {
                return redirect()->route('admin.bars.edit', [$bar->id, '#offers']);
            }
        }

        return view('admin.offers.edit', compact('model', 'bar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Bar $bar
     * @param  int $id
     * @return Response
     */
    public function update($bar, $id)
    {
        $bar = Bar::find($bar);
        if ($bar) {
            $input = $this->request->all();

            $validator = Validator::make($input, Offer::rules($id));
            if ($validator->fails()) {
                return redirect()->route('admin.bars.offers.edit', [$bar->id, $id])
                    ->withErrors($validator)
                    ->withInput();
            }

            $model = Offer::find($id);
            $model->update($input);
        }
        return redirect()->route('admin.bars.edit', [$bar->id, '#offers']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bar $bar
     * @param  int $id
     * @return Response
     */
    public function destroy($bar, $id)
    {
        Offer::find($id)->delete();
        return response()->json();
    }

}
