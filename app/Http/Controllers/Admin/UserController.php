<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\User;
use App\Models\Role;
use Validator;

class UserController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::where('email', '<>', '')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::lists('name', 'id');
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, User::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.users.create')
                ->withErrors($validator)
                ->withInput();
        }


        $user = new User ();
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->role_id = $input['role_id'];
        $user->save();

        return redirect()->route('admin.users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = User::find($id);
        $points = [];
        if ($model) {
            $points = $model->pointsLog()->orderBy('id', 'desc')->getResults();
        }
        return view('admin.users.show', compact('model', 'points'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = User::find($id);
        $roles = Role::lists('name', 'id');
        if($model === null){
            return redirect('admin/users');
        }
        return view('admin.users.edit', compact('model', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();

        $validator = Validator::make($input, User::$rulesUpdate);
        if ($validator->fails()) {
            return redirect()->route('admin.users.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = User::find($id);
        if (!empty($input['name'])) {
            $model->name = $input['name'];
        }
        if (!empty($input['email'])) {
            $model->email = $input['email'];
        }
        if (!empty($input['password'])) {
            $model->password = bcrypt($input['password']);
        }
        if (!empty($input['role_id'])) {
            $model->role_id = $input['role_id'];
        }
        $model->save();
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return response()->json();
    }

}
