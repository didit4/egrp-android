<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Catalog;
use App\Models\SportEvent;
use App\Models\Team;
use Illuminate\Support\Facades\Response;
use Validator;

class SportEventController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $events = SportEvent::all();
        return view('admin.events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $teams = Team::lists('name', 'id');
        $tournament = Catalog::where('name', 'Турниры')->first();
        $tournaments = [];
        if ($tournament) {
            $tournaments = $tournament->getListForDropdown('Выберите турнир');
        }
        return view('admin.events.create', compact('teams', 'tournaments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, SportEvent::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.sport-events.create')
                ->withErrors($validator)
                ->withInput();
        }

        SportEvent::create($input);

        return redirect()->route('admin.sport-events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $model = SportEvent::find($id);
        return view('admin.events.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = SportEvent::find($id);
        if($model === null){
            return redirect('admin/sport-events');
        }
        $teams = Team::lists('name', 'id');
        $tournament = Catalog::where('name', 'Турниры')->first();
        $tournaments = [];
        if ($tournament) {
            $tournaments = $tournament->getListForDropdown('Выберите турнир');
        }
        return view('admin.events.edit', compact('model', 'teams', 'tournaments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();

        $validator = Validator::make($input, SportEvent::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.sport-events.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = SportEvent::find($id);
        $model->update($input);
        return redirect('admin/sport-events');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        SportEvent::find($id)->delete();
        return response()->json();
    }

    public function switch_active($id)
    {
        $game = SportEvent::find($id);
        if($game === null){
            return redirect('admin/sport-events');
        }
        $game->active = !$game->active;
        $game->save();
        return redirect('admin/sport-events');
    }

}
