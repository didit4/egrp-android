<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Http\Requests;
use App\Models\Rule;
use Illuminate\Support\Facades\Response;
use Validator;

class RuleController extends AdminController
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $rule = Rule::first();
        return view('admin.rule.index', compact('rule'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.rule.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = $this->request->all();

        $validator = Validator::make($input, Rule::$rules);
        if ($validator->fails()) {
            return redirect()->route('admin.rules.create')
                ->withErrors($validator)
                ->withInput();
        }

        Rule::create($input);

        return redirect()->route('admin.rules.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $model = Rule::find($id);
        if($model === null){
            return redirect('admin/rules');
        }
        return view('admin.rule.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = $this->request->all();
        $validator = Validator::make($input, Rule::rules($id));
        if ($validator->fails()) {
            return redirect()->route('admin.rules.edit', ['id' => $id])
                ->withErrors($validator)
                ->withInput();
        }

        $model = Rule::find($id);
        $model->update($input);
        return redirect('admin/rules');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Bar::find($id)->delete();
        return response()->json();
    }

}
