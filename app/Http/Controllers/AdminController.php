<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Validator;

class AdminController extends Controller
{

    public function __construct(Request $request)
    {
        $this->request = $request;
//        $this->middleware('auth');
        View::share('staticversion', Config::get('static.version'));
    }

    public function index()
    {
        return redirect()->route('admin.users.index');
    }
}
