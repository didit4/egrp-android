<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use App\Models\PayType;
use App\Models\Place;
use App\Models\Region;
use App\Models\Req;
use App\Models\ReqType;
use App\Models\Setting;
use App\Models\StType;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;
use Lang;
use Mail;

class ReqController extends BaseController
{

    public function get(Request $request)
    {
        $number = $request->input('number');
        $phone = str_replace('-', '', str_replace(' ', '', trim((string)$request->input('phone'))));

        if (strpos($phone, '+') === 0) {
            if (strpos($phone, '8') === 1) {
                $phone = '7' . substr($phone, 1);
            }
        } else {
            if (strpos($phone, '8') === 0) {
                $phone = '7' . substr($phone, 1);
            }
            $phone = '+' . $phone;
        }

        if (!$number || !$phone) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.req_valid')
            ], 422);
        }

        $req = Req::where('id', $number)->where('phone', $phone)->first();
        if ($req === null) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.req_not_found')
            ], 422);

        }
        return response()->json(['req' => $req], 200);
    }

    public function get_regions()
    {
        $regions = Region::orderBy('sort', 'DESC')->orderBy('name', 'ASC')->get();
        if (!$regions) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.req_not_found')
            ], 422);
        }
        $arrRegions = [];
        foreach($regions as $region) {
            $arrRegions[] = $region;
        }
        return response()->json(['regions' => $arrRegions], 200);
    }

    public function get_areas(Request $request)
    {
        $regionId = $request->input('region_id');
        if (!$regionId) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.req_valid')
            ], 422);
        }
        if ($regionId == 50857) {
            return response()->json(['areas' => []], 200);
        }

        $places = Place::where('region_place_id', $regionId)
            ->where('hide', 0)
            ->where(function($query)
            {
                $query->where('type', 2)
                    ->orWhere('type', 3);
            })
            ->orderBy('name', 'ASC')->get();
        $arrPlaces = [];
        foreach($places as $place) {
            $arrPlaces[] = $place;
        }
        return response()->json(['areas' => $arrPlaces], 200);
    }

    public function get_places(Request $request)
    {
        $areaId = $request->input('area_id');
        if (!$areaId) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.req_valid')
            ], 422);
        }

        $places = Place::where('parent_place_id', $areaId)
            ->where('hide', 0)
            ->where(function($query)
            {
                $query->where('type', '!=', 2)
                    ->where('type', '!=', 3);
            })
            ->orderBy('name', 'ASC')->get();
        $arrPlaces = [];
        foreach($places as $place) {
            $arrPlaces[] = $place;
        }
        return response()->json(['places' => $arrPlaces], 200);
    }


    public function get_type_st()
    {
        $places = StType::where('disable', false)
            ->orderBy('sort', 'DESC')
            ->orderBy('name', 'ASC')
            ->get();
        if (!$places) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.req_not_found')
            ], 422);
        }
        $arrPlaces = [];
        foreach($places as $place) {
            $arrPlaces[] = $place;
        }
        return response()->json(['st_types' => $arrPlaces], 200);
    }

    public function new_request(Request $request)
    {
        $attributes = [
            'name' => $request->input('name'),
            'phone' => str_replace('-', '', str_replace(' ', '', trim((string)$request->input('phone')))),
            'email' => $request->input('email'),
            'price' => $request->input('price'),
            'payType' => $request->input('payType')
        ];
        if($request->has('region_id')) {
            $attributes['region_id'] = $request->input('region_id');
            if ($request->has('area_id')) {
                $attributes['area_id'] = $request->input('area_id');
            }
            if ($request->has('locality_id')) {
                $attributes['locality_id'] = $request->input('locality_id');
            }
            if ($request->has('street_type')) {
                $attributes['street_type'] = $request->input('street_type');
            }
            if ($request->has('street_name')) {
                $attributes['street_name'] = $request->input('street_name');
            }
            if ($request->has('house')) {
                $attributes['house'] = $request->input('house');
            }
            $region = Region::where('place_id', $request->input('region_id'))->first();
            $attributes['region_name'] = $region->getFullName();
            if ($request->has('area_id')) {
                $area = Place::where('place_id', $request->input('area_id'))->first();
                $attributes['area_name'] = $area->getFullName();
            }
            if ($request->has('locality_id')) {
                $locality = Place::where('place_id', $request->input('locality_id'))->first();
                $attributes['locality_name'] = $locality->getFullName();
            }
            if ($request->has('street_type')) {
                $stType = StType::where('id', $request->input('street_type'))->first();
                $attributes['street_type_name'] = $stType->name;
            }
        }
        if($request->has('cadastral_num')) {
            $attributes['cadastral_num'] = $request->input('cadastral_num');
        }elseif($request->has('conditional_num')) {
            $attributes['conditional_num'] = $request->input('conditional_num');
        }elseif($request->has('srr_num')) {
            $attributes['srr_num'] = $request->input('srr_num');
        }elseif($request->has('limit_num')) {
            $attributes['limit_num'] = $request->input('limit_num');
        }

        if($request->has('room')) {
            $attributes['room'] = $request->input('room');
        }
        if($request->has('building')) {
            $attributes['building'] = $request->input('building');
        }
        if($request->has('housing')) {
            $attributes['housing'] = $request->input('housing');
        }

        $attributes = Req::fillFristFields($attributes);

        if($request->has('pay_type')) {
            $attributes['commission'] = PayType::where('type', $request->input('pay_type'))->first()->percent;
        }

        if($request->has('type')) {
            $attributes['type'] = $request->input('type');
        }

        if($request->has('address')) {
            $attributes['address'] = $request->input('address');
        }

        $attributes['notpayed'] = !$request->has('web');

        $rules = Req::$rules;
        if(!$request->has('region_id')) {
            $rules = Req::$rulesNums;
        }
        $validator = Validator::make($attributes, $rules);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.req_valid')
            ], 422);
        }

        $req = Req::create($attributes);

        $email = $request->input('email');
        Mail::send('emails.request_create', ['id' => $req->id, 'name' => $request->input('name')], function ($message) use($email) {

            $message->from('hello@egrp.today', 'egrp.today');
            $message->to($email)->subject('Ваша заявка на выписку ЕГРП с egrp.today');

        });

        return response()->json([
            'req' => (string) $req->id
        ], 200);
    }

    public function get_price()
    {
        $price = Setting::find(1);
        return response()->json(['price' => (string) $price->value], 200);
    }

    public function get_types()
    {
        $types = ReqType::orderBy('order', 'DESC')->get();
        $arrTypes = [];
        foreach($types as $type) {
            $arrTypes[] = $type;
        }
        return response()->json(['types' => $arrTypes], 200);
    }

}
