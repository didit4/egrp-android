<?php namespace App\Http\Controllers\Api\V1;

use App\Http\Requests;
use App\Models\Contacts;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;

class ContactsController extends BaseController
{

    public function get()
    {
        $contacts = Contacts::first();
        return response()->json(['contacts' => $contacts], 200);
    }

    public function offer()
    {
        $contacts = Contacts::where('id', 2)->first();
        return response()->json(['offer' => $contacts], 200);
    }

}
