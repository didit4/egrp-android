<?php namespace App\Http\Controllers\Api\V1;

use App\Models\Question;
use App\Models\Req;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator;
use Lang;

class QuestionController extends BaseController
{

    public function send(Request $request)
    {
        $input = $request->only(['email', 'name', 'text']);

        $validator = Validator::make($input, Question::$rules);
        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.question_valid')
            ], 422);
        }
        Question::create($input);

        return response()->json(['message' => Lang::get('messages.question_send')], 200);
    }

}
