<?php namespace App\Http\Controllers\Api\V1;

use JWTAuth;
use Validator;
use Config;
use Lang;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends ApiController
{
    public function login(Request $request)
    {
        if ($request->get('vk_id')) {
            $credentials = $request->only(['vk_id', 'token']);
            $validator = Validator::make($credentials, [
                'vk_id' => 'required',
            ]);
        } elseif ($request->get('fb_id')) {
            $credentials = $request->only(['fb_id', 'token']);
            $validator = Validator::make($credentials, [
                'fb_id' => 'required',
            ]);
        } else {
            $credentials = $request->only(['email', 'password']);
            $validator = Validator::make($credentials, [
                'email' => 'required',
                'password' => 'required',
            ]);
        }

        if($validator->fails()) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.auth_validate_error')
			], 422);
        }
        try {
            if ($request->get('vk_id')) {
                if (md5($request->get('vk_id') . Config::get('app.token_salt')) === $credentials['token']) {
                    $user = User::where('vk_id', $credentials['vk_id'])
                        ->first();
                } else {
                    return response()->json([
                        'code' => 422,
                        'error' => Lang::get('messages.auth_validate_error')
                    ], 422);
                }
            } elseif ($request->get('fb_id')) {
                if (md5($request->get('fb_id') . Config::get('app.token_salt')) === $credentials['token']) {
                    $user = User::where('fb_id', $credentials['fb_id'])
                        ->first();
                } else {
                    return response()->json([
                        'code' => 422,
                        'error' => Lang::get('messages.auth_validate_error')
                    ], 422);
                }
            } else {
                $user = null;
            }
            if (! $token = JWTAuth::fromUser($user)) {
                return $this->response->errorUnauthorized();
            }
        } catch (JWTException $e) {
            return $this->response->error([
                'code' => 500,
                'error' => Lang::get('messages.could_not_create_token')
            ], 500);
        }

        return response()->json(compact('user', 'token'));
    }

    public function signup(Request $request)
    {
        if ($request->get('vk_id')) {
            $signupFields = [
                'name',
                'vk_id',
                'photo',
            ];
            $hasToReleaseToken = [
                'name' => 'required',
                'vk_id' => 'required|unique:users',
                'photo' => 'required',
            ];
        } elseif ($request->get('fb_id')) {
            $signupFields = [
                'name',
                'fb_id',
                'photo',
            ];
            $hasToReleaseToken = [
                'name' => 'required',
                'fb_id' => 'required|unique:users',
                'photo' => 'required',
            ];
        } else {
            $signupFields = [
                'name',
                'email',
                'password'
            ];
            $hasToReleaseToken = [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6'
            ];
        }

        $user = null;
        if ($request->get('vk_id')) {
            if (md5($request->get('vk_id') . Config::get('app.token_salt')) === $request->get('token')) {
                $user = User::where('vk_id', $request->get('vk_id'))
                    ->first();
            }
        } elseif ($request->get('fb_id')) {
            if (md5($request->get('fb_id') . Config::get('app.token_salt')) === $request->get('token')) {
                $user = User::where('fb_id', $request->get('fb_id'))
                    ->first();
            }
        }

        if ($user) {
            if (! $token = JWTAuth::fromUser($user)) {
                return $this->response->errorUnauthorized();
            }
            return response()->json(compact('user', 'token'));
        }

        $userData = $request->only($signupFields);
        $validator = Validator::make($userData, $hasToReleaseToken);
        if($validator->fails()) {
            return response()->json([
                'code' => 422,
				'error' => Lang::get('messages.reg_validate_error')
			], 422);
        }
        User::unguard();
        $user = User::create($userData);
        User::reguard();
        if(!$user->id) {
            return $this->response->error([
                'code' => 422,
				'error' => Lang::get('messages.could_not_create_user')
			], 422);
        }
        if($hasToReleaseToken) {
            return $this->login($request);
        }

        return $this->response->created();
    }

    public function checkUser(Request $request)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json(compact('user'));
    }

    public function attach(Request $request)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }

        $signupFields = [];
        $hasToReleaseToken = [];
        if ($request->get('vk_id')) {
            $signupFields = [
                'vk_id',
            ];
            $hasToReleaseToken = [
                'vk_id' => 'required|unique:users',
            ];
        } elseif ($request->get('fb_id')) {
            $signupFields = [
                'fb_id',
            ];
            $hasToReleaseToken = [
                'fb_id' => 'required|unique:users',
            ];
        }

        if(!count($signupFields) || !count($hasToReleaseToken) ) {
            return response()->json([
                'code' => 422,
				'error' => Lang::get('messages.attach_error')
			], 422);
        }

        $user = JWTAuth::parseToken()->authenticate();

        if ($request->get('vk_id') && $user->vk_id || $request->get('fb_id') && $user->fb_id) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.attach_error_exist')
            ], 422);
        }

        $userData = $request->only($signupFields);
        $validator = Validator::make($userData, $hasToReleaseToken);
        if($validator->fails()) {
            return response()->json([
                'code' => 422,
                'error' => Lang::get('messages.attach_error')
            ], 422);
        }

        if ($request->get('vk_id')) {
            $user->vk_id = $request->get('vk_id');
        }elseif ($request->get('fb_id')) {
            $user->fb_id = $request->get('fb_id');
        }
        $user->save();

        return response()->json([], 204);
    }

    public function change_token_access(Request $request)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }

        $user = JWTAuth::parseToken()->authenticate();
        $error = false;
        if ($request->get('access_token_vk')) {
            if ($user->access_token_vk) {
                $user->access_token_vk = $request->get('access_token_vk');
                $user->save();
            } else {
                $error = true;
            }
        } elseif ($request->get('access_token_fb')) {
            if ($user->access_token_fb) {
                $user->access_token_fb = $request->get('access_token_fb');
                $user->save();
            } else {
                $error = true;
            }
        } else {
            $error = true;
        }
        if ($error) {
            return response()->json([
                'code' => 422,
				'error' => Lang::get('messages.token_change_error')
			], 422);
        }

        return response()->json([], 204);
    }

    public function logout(Request $request)
    {
        if (! $token = JWTAuth::parseToken()) {
            return $this->response->errorUnauthorized();
        }
        JWTAuth::refresh();
        return response()->json([], 204);
    }
}
