<?php namespace App\Http\Controllers;

use App\Models\Contacts;
use App\Models\Faq;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('welcome');
	}

	public function offer()
	{
        $contact = Contacts::where('id', 2)->first();
        $email = Contacts::where('id', 1)->first();

        return view('offer', compact('contact', 'email'));
	}

	public function faq()
	{
        $arrFaqs = Faq::where('active', true)->orderBy('sort', 'asc')->get();
        $faqs = [];
        foreach ($arrFaqs as $faq) {
            $faqs[] = $faq;
        }
        $email = Contacts::where('id', 1)->first();

        return view('faq', compact('faqs', 'email'));
	}

    public function status()
    {
        $contact = Contacts::where('id', 2)->first();
        $email = Contacts::where('id', 1)->first();

        return view('status', compact('contact', 'email'));
	}

}
