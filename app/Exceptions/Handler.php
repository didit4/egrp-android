<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Support\Facades\Response;
use Lang;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
			return response()->json([
				'code' => $e->getStatusCode(),
				'error' => Lang::get('messages.token_expired')
			], $e->getStatusCode());
		} else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
			return response()->json([
				'code' => $e->getStatusCode(),
				'error' => Lang::get('messages.token_invalid')
			], $e->getStatusCode());
		}

		if (strpos($request->getPathInfo(), '/api/') !== false) {
			//Handle Route Not Found
			if ($e instanceof NotFoundHttpException) {
				//Ajax Requests
				if ($request->ajax() || $request->wantsJson() || strpos($request->getPathInfo(), '/api/') !== false) {
					$message = Lang::get('messages.route_not_defined');
					if ($e->getMessage()) {
						$message = $e->getMessage();
					}
					$data = [
						'code' => 404,
						'error' => $message
					];

					return response()->json([$data], 404);
				}
			}
			//Handle HTTP Method Not Allowed
			if ($e instanceof MethodNotAllowedHttpException) {
				//Ajax Requests
				if ($request->ajax() || $request->wantsJson()) {
					$data = [
						'code' => 401,
						'error' => Lang::get('messages.method_not_allowed'),
					];

					return response()->json([$data], 401);
				}
				/*else
                {
                    //Return view
                    return response()->view('404');
                }*/
			}

			if ($this->isHttpException($e)) {
				return response()->json([
					'code' => 422,
					'error' => $e->getMessage() ? : Lang::get('messages.unknown_error')
				], 422);
			} else {
				return response()->json([
					'code' => 500,
					'error' => $e->getMessage() ? : Lang::get('messages.server_error')
				], 500);
			}
		}

		return parent::render($request, $e);
	}

}
