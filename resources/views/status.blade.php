<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>ЕГРН сегодня</title>
	<link rel="apple-touch-icon" href="/request/img/apple-touch-icon.png">
	<link rel="icon" href="/request/img/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/request/img/favicon.ico" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet">

	<!-- Styles -->
	<link rel="stylesheet" href="/request/css/main.css">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>
	<![endif]-->

	<meta name="yandex-verification" content="a015689f210da137" />
</head>
<body class="">


<header class="header header_inner">
	<div class="header__container">
		<a href="/" class="header__logo">
			<img src="/request/img/logo.png" alt=""/>
		</a>
	</div>
</header>
<div id="content" class="fd-wrapper">
	<main class="fd-wrapper__content">
		<div class="fd-wrapper__container">

			<!--blocks-->

			<div class="fd-etabs__wrapper">
				<h1 class="fd-etabs__title">Узнать статус выписки</h1>
				<form id="form3">
					<h2 class="fd-etabs__title-3">ВВЕДИТЕ ДАННЫЕ заявки, чтобы узнать статус:</h2>

					<div class="input-field">
						<input id="input-status-1" name="input-status-1" type="number" class="validate" data-name="" min="1">
						<label for="input-status-1">Номер заявки</label>
					</div>
					<div class="input-field">
						<input id="input-9" name="input-9" type="text" class="validate">
						<label for="input-9">Телефон</label>
					</div>
					<div class="fd-button__wrapper"><a href="#" class="fd-button get-status">Узнать статус</a></div>
				</form>
				<div class="result-request" style="display: none">
					<h2 class="fd-etabs__title-3">ДАННЫЕ по заявке №3 // +7 (911) 323-32-45</h2>

					<!--blocks-->
					<div class="fd-status">
						<ul class="fd-status__list">
							<li class="fd-status__list-item">
								<div class="fd-status__dot"></div>
								<div class="fd-status__body">
									Новая заявка получена
									<div class="fd-status__date"></div>
								</div>
							</li>
							<li class="fd-status__list-item">
								<div class="fd-status__dot"></div>
								<div class="fd-status__body">
									Обрабатывается
									<div class="fd-status__date"></div>
								</div>
							</li>
							<li class="fd-status__list-item ">
								<div class="fd-status__dot"></div>
								<div class="fd-status__body">
									В Росреестре
									<div class="fd-status__date"></div>
								</div>
							</li>
							<li class="fd-status__list-item ">
								<div class="fd-status__dot"></div>
								<div class="fd-status__body">
									Получен XML
									<div class="fd-status__date"></div>
								</div>
							</li>
							<li class="fd-status__list-item ">
								<div class="fd-status__dot"></div>
								<div class="fd-status__body">
									<span>Отправлено клиенту на email</span>
									<div class="fd-status__date"></div>
								</div>
							</li>
						</ul>
					</div>

				</div>

			</div>

		</div>
	</main>


</div>
<footer class="footer">
	<div class="footer__container">
		<div class="footer__row">
			<div class="footer__col">
				<div class="footer__copy">©2017 Все права защищены.</div>
			</div>
			<div class="footer__col footer__col_right">
				<nav class="footer-menu">
					<ul class="footer-menu__list">
						<li class="footer-menu__list-item">
							<a href="/status" class="footer-menu__list-link">Узнать статус заявки</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="mailto:{{$contact->email}}" class="footer-menu__list-link">Напишите нам</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/faq" class="footer-menu__list-link">Вопросы и ответы</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/offer" class="footer-menu__list-link">Оферта</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>

<!-- Scripts -->
<script src="/request/js/vendor.js"></script>
<script src="/request/js/main.js"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter45125202 = new Ya.Metrika({
					id:45125202,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true,
					webvisor:true,
					trackHash:true
				});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45125202" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
