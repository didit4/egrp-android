<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>ЕГРН сегодня</title>
	<link rel="apple-touch-icon" href="/request/img/apple-touch-icon.png">
	<link rel="icon" href="/request/img/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/request/img/favicon.ico" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet">

	<!-- Styles -->
	<link rel="stylesheet" href="/request/css/main.css?v=290617">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>
	<![endif]-->

	<meta name="yandex-verification" content="a015689f210da137" />
</head>
<body class="">


<header class="header ">
	<div class="header__container">
		<a href="/" class="header__logo">
			<img src="/request/img/logo.png" alt=""/>
		</a>
	</div>
</header>
<div id="content" class="fd-wrapper">
	<!--<div class="fd-wrapper__table">-->
	<div class="fd-wrapper__table-cell fd-wrapper__table-cell_bottom">
		<main class="fd-wrapper__content">
			<div class="fd-wrapper__container">

				<!--blocks-->

				<div class="fd-advantages__wrapper">
					<div class="fd-advantages__row">
						<div class="fd-advantages__col">
							<div class="fd-advantages">
								<div class="fd-advantages__title">УДОБНО</div>
								<div class="fd-advantages__text">
									3 простых шага
								</div>
								<div class="fd-advantages__img"><img src="/request/img/advantages-1.png" alt=""></div>
							</div>
						</div>
						<div class="fd-advantages__col">
							<div class="fd-advantages">
								<div class="fd-advantages__title">БЫСТРО</div>
								<div class="fd-advantages__text">
									от 10 минут, время — деньги
								</div>
								<div class="fd-advantages__img"><img src="/request/img/advantages-2.png" alt=""></div>
							</div>
						</div>
						<div class="fd-advantages__col">
							<div class="fd-advantages">
								<div class="fd-advantages__title">ВЫГОДНО</div>
								<div class="fd-advantages__text">
									всего 242 &#8381;
								</div>
								<div class="fd-advantages__img"><img src="/request/img/advantages-3.png" alt=""></div>
							</div>
						</div>
					</div>
				</div>
				<div class="morph-button morph-button-modal morph-button-modal-2 morph-button-fixed">
					<button type="button" class="fd-button fd-button_morph">Заказать выписку официально через сайт</button>
					<div class="morph-content">
						<div>
							<div class="icon icon-close"><span></span></div>
							<!--blocks-->

							<div class="fd-etabs__wrapper">
								<div class="first-step">
									<h1 class="fd-etabs__title">Сначала выберите тип объекта</h1>
									<div class="types">

									</div>
									<div class="fd-button__wrapper fd-button__wrapper_mt"><a href="#" class="fd-button goto-start">Перейти к заявке</a></div>
								</div>
								<div class="main-form" style="display: none">
									<h1 class="fd-etabs__title">Заказ выписки</h1>
									<div class="fd-tab-container">
										<ul class='fd-etabs'>
											<li class='fd-tab'><a href="#fd-step-1">ШАГ 1. ЗАЯВКА</a></li>
											<li class='fd-tab'><a href="#fd-step-2" class="disabled step-2">ШАГ 2. Контакты</a></li>
											<li class='fd-tab'><a href="#fd-step-3" class="disabled step-3">ШАГ 3. ОПЛАТА</a></li>
										</ul>


										<div id="fd-step-1" class="fd-etabs__item">

											<div class="fd-tab-container">
												<h2 class="fd-etabs__title-2">Выберите способ заказа выписки</h2>
												<ul class='fd-etabs second'>
													<li class='fd-tab'><a href="#fd-step-1-1">ПО КАДАСТРОВОМУ НОМЕРУ</a></li>
													<li class='fd-tab'><a href="#fd-step-1-2">ПО АДРЕСУ ОБЪЕКТА</a></li>
												</ul>
												<form id="form1">
													<div id="fd-step-1-1" class="fd-etabs__item">
														<div class="input-field">
															<input id="input-1" name="input-1" type="text" class="validate" data-name="cadastral_num">
															<label for="input-1">Введите кадастровый номер</label>
														</div>
													</div>
													<div id="fd-step-1-2" class="fd-etabs__item">
														<div class="input-field">
															<select name="select-1" id="select-1">
																<option value="">Выберите регион</option>
																<option value="1">Option 1</option>
																<option value="2">Option 2</option>
																<option value="3">Option 3</option>
															</select>
														</div>

														<div class="fd-etabs__row">
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<select name="select-2" id="select-2" disabled>
																		<option value="">Выберите район</option>
																		<option value="1">Option 1</option>
																		<option value="2">Option 2</option>
																		<option value="3">Option 3</option>
																	</select>
																</div>
															</div>
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<select name="select-3" id="select-3" disabled>
																		<option value="">Выберите населённый пункт</option>
																		<option value="1">Option 1</option>
																		<option value="2">Option 2</option>
																		<option value="3">Option 3</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="fd-etabs__row">
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<select name="select-4" id="select-4">
																		<option value="">Выберите тип улицы</option>
																		<option value="1">Option 1</option>
																		<option value="2">Option 2</option>
																		<option value="3">Option 3</option>
																	</select>
																</div>
															</div>
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<input id="input2" name="input2" type="text" class="" required
																		   data-name="street_name">
																	<label for="input2">Введите название улицы</label>
																</div>
															</div>
														</div>
														<div class="fd-etabs__row">
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<input id="input3" name="input3" type="text" class="" data-name="house">
																	<label for="input3">Номер дома</label>
																</div>
															</div>
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<input id="input-4" name="input-4" type="text" class="validate" data-name="room">
																	<label for="input-4">Квартира</label>
																</div>
															</div>
														</div>
														<div class="fd-etabs__row">
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<input id="input-5" name="input-5" type="text" class="validate"
																		   data-name="building">
																	<label for="input-5">Строение</label>
																</div>
															</div>
															<div class="fd-etabs__col-2">
																<div class="input-field">
																	<input id="input-6" name="input-6" type="text" class="validate" data-name="housing">
																	<label for="input-6">Корпус</label>
																</div>
															</div>
														</div>


													</div>
												</form>
											</div>
											<div class="fd-button__wrapper"><a href="#" class="fd-button goto2">Дальше. Перейти к шагу 2</a></div>


										</div>
										<div id="fd-step-2" class="fd-etabs__item">
											<h2 class="fd-etabs__title-2 fd-etabs__title-2_grey">ВВЕДИТЕ ВАШИ ДАННЫЕ ДЛЯ СВЯЗИ:</h2>
											<form id="form2">
												<div class="input-field">
													<input id="input-7" name="input-7" type="text" class="validate">
													<label for="input-7">Имя</label>
												</div>
												<div class="input-field">
													<input id="input-8" name="input-8" type="text" class="validate">
													<label for="input-8">Email</label>
												</div>
												<div class="input-field">
													<input id="input-9" name="input-9" type="text" class="validate">
													<label for="input-9">Телефон</label>
												</div>
											</form>
											<div class="fd-button__wrapper"><a href="#" class="fd-button goto3">дальше шаг 3</a></div>
											<!-- content -->
										</div>
										<div id="fd-step-3" class="fd-etabs__item">
											<h2 class="fd-etabs__title-2">Стоимость заявки <span class="sum_txt">400</span> руб</h2>
											<div class="fd-button__wrapper" style="">
												<form id="form-ac" action="https://money.yandex.ru/eshop.xml" method="post">
													<input name="shopId" value="126358" type="hidden"/>
													<input name="scid" value="93383" type="hidden"/>
													<input name="sum" value="400" type="hidden">
													<input name="customerNumber" value="abc1111111" type="hidden"/>
													<input name="paymentType" value="AC" type="hidden"/>
													<input name="orderNumber" value="abc1111111" type="hidden"/>
													<input type="hidden" name="cps_email" value="">
													<input type="hidden" name="cps_phone" value="">
													<input name="custName" value="" type="hidden"/>
													<input type="submit" class="fd-button create-order-btn" value="Оплатить через Яндекс"/>
												</form>
											</div>
											<h2 class="fd-etabs__title-2">Все платежи защищены.</h2>
											<h2 class="fd-etabs__title-2">Нажимая оплатить, вы принимаете <a style="color: #ff9500" href="/offer" target="_blank">оферту</a></h2>
											<!-- content -->
										</div>
									</div>
								</div>
							</div>




						</div>
					</div>


				</div>
				<div class="fd-text fd-text_full">
					<div class="fd-text__link-wrapper">
						<div class="fd-text__link-item"><p>или в приложении</p></div>
					</div>
					<div class="fd-text__link-wrapper">
						<div class="fd-text__link-item">
							<a href="https://appsto.re/ru/OTQijb.i" class="fd-text__link"><img src="/request/img/app-store.png" alt=""></a>
						</div>
						<div class="fd-text__link-item">
							<a href="https://play.google.com/store/apps/details?id=com.mediapark.egrptoday" class="fd-text__link"><img src="/request/img/google-play.png" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
	<!--</div>-->

</div>
<footer class="footer">
	<div class="footer__container">
		<div class="footer__row">
			<div class="footer__col">
				<div class="footer__copy">©2017 Все права защищены.</div>
			</div>
			<div class="footer__col footer__col_right">
				<nav class="footer-menu">
					<ul class="footer-menu__list">
						<li class="footer-menu__list-item">
							<a href="/status" class="footer-menu__list-link">Узнать статус заявки</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="mailto:{{$contact->email}}" class="footer-menu__list-link">Напишите нам</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/faq" class="footer-menu__list-link">Вопросы и ответы</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/offer" class="footer-menu__list-link">Оферта</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>

<!-- Scripts -->
<script src="/request/js/vendor.js?v=130617"></script>
<script src="/request/js/main.js?v=0207174"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter45125202 = new Ya.Metrika({
					id:45125202,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true,
					webvisor:true,
					trackHash:true
				});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45125202" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
