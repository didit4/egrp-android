<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>ЕГРН сегодня</title>
	<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i,700,700i&subset=cyrillic"
		  rel="stylesheet">

	<!-- Styles -->
	<link rel="stylesheet" href="/order/css/main.css">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>
	<![endif]-->
</head>
<body class="">


<header class="header header_inner">
	<div class="header__container">
		<a href="/" class="header__logo">
			<img src="/order/img/logo.png" alt=""/>
		</a>
	</div>
</header>
<div id="content" class="fd-wrapper">
	<main class="fd-wrapper__content">
		<div class="fd-wrapper__container">
			<!--blocks-->
			<div class="fd-etabs__wrapper">
				<div class="fd-tab-container">
					<ul class='fd-etabs'>
						<li class='fd-tab'><a href="#fd-step-1">Шаг 1</a></li>
						<li class='fd-tab'><a class="disabled" href="#fd-step-2">Шаг 2</a></li>
						<li class='fd-tab'><a class="disabled" href="#fd-step-3">Шаг 3</a></li>
					</ul>


					<div id="fd-step-1">

						<div class="fd-tab-container">
							<h2>Выберите способ заказа выписки</h2>
							<ul class='fd-etabs'>
								<li class='fd-tab'><a href="#fd-step-1-1">По кадастровому номеру</a></li>
								<li class='fd-tab'><a href="#fd-step-1-2">По адресу объекта</a></li>
							</ul>

							<div id="fd-step-1-1">
								<div class="input-field">
									<input id="input-1" type="text" class="validate">
									<label for="input-1">Введите кадастровыйномер</label>
								</div>
							</div>
							<div id="fd-step-1-2">
								<select>
									<option value="">Регион</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>
								<select>
									<option value="">Район</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>
								<select>
									<option value="">Населенный пункт</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>
								<select>
									<option value="">Тип улицы</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>

								<div class="input-field">
									<input id="input-2" type="text" class="validate">
									<label for="input-2">Название улицы</label>
								</div>
								<div class="input-field">
									<input id="input-3" type="text" class="validate">
									<label for="input-3">Номер дома</label>
								</div>
								<div class="input-field">
									<input id="input-4" type="text" class="validate">
									<label for="input-4">Квартира</label>
								</div>
								<div class="input-field">
									<input id="input-5" type="text" class="validate">
									<label for="input-5">Строение</label>
								</div>
								<div class="input-field">
									<input id="input-6" type="text" class="validate">
									<label for="input-6">Корпус</label>
								</div>
							</div>
						</div>
						<div class="fd-batton__wrapper"><a href="#" class="fd-batton" >дальше шаг 2</a></div>



					</div>
					<div id="fd-step-2">
						<h2>Введите ваши данные для связи</h2>
						<div class="input-field">
							<input id="input-7" type="text" class="validate">
							<label for="input-7">Имя</label>
						</div>
						<div class="input-field">
							<input id="input-8" type="text" class="validate">
							<label for="input-8">Email</label>
						</div>
						<div class="input-field">
							<input id="input-9" type="text" class="validate">
							<label for="input-9">Телефон</label>
						</div>
						<div class="fd-batton__wrapper"><a href="#" class="fd-batton" >дальше шаг 3</a></div>
						<!-- content -->
					</div>
					<div id="fd-step-3">
						<h2>400 руб. Выберите способ оплаты</h2>
						<div class="fd-batton__wrapper" style="text-align: center;"><a href="#" class="fd-batton" >Банковткая карта</a></div>
						<div class="fd-batton__wrapper" style="text-align: center;"><a href="#" class="fd-batton" >Оплата через Яндекс</a></div>
						<!-- content -->
					</div>
				</div>
			</div>
		</div>
	</main>

</div>
<footer class="footer">
	<div class="footer__container">
		<div class="footer__row">
			<div class="footer__col">
				<div class="footer__copy">©2017 Все права защищены.</div>
			</div>
			<div class="footer__col footer__col_right">
				<nav class="footer-menu">
					<ul class="footer-menu__list">
						<li class="footer-menu__list-item">
							<a href="mailto:{{$email->email}}" class="footer-menu__list-link">Напишите нам</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/faq" class="footer-menu__list-link">Вопросы и ответы</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/offer" class="footer-menu__list-link">Оферта</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>

<!-- Scripts -->
<script src="/order/js/vendor.js"></script>
<script src="/order/js/main.js"></script>
</body>
</html>
