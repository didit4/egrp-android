<div class="page-header">
    <h3>Офферы</h3>
    @if(!$offers->count())
        <em class="bg-warning">Ни одного оффера еще не добавлено. Нажмите кнопку "Добавить", чтобы исправить это.</em>
    @endif
</div>

<div class="row">
    <div class="col-xs-8">
        {!! HTML::linkRoute('admin.bars.offers.create', 'Добавить', [$bar->id], ['class' => 'btn btn-md btn-success']) !!}
    </div>
</div>

<table id="table" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Действия</th>
        <th>Название</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($offers as $offer)
        <tr>
            <td>
                <div class="btn-group btn-group-xs" role="group">
                    <a href="{!!  URL::route('admin.bars.offers.edit', [$bar->id, $offer->id]) !!}" class="btn btn-warning"
                       title="Редактировать">
                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                    </a>
                    <a href="{!!  URL::route('admin.bars.offers.destroy', [$bar->id, $offer->id]) !!}"
                       class="btn btn-danger deleteObject" title="Удалить">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </a>
                </div>
            </td>
            <td>
                {{ $offer->name }}
            </td>
        </tr>
    @endforeach

    </tbody>
</table>
