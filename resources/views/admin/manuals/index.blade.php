@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Мануал @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Мануал</h3>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if ($manual)
                {!! HTML::linkRoute('admin.manuals.edit', 'Редактировать', [$manual->id], ['class' => 'btn btn-md btn-default']) !!}
            @else
                {!! HTML::linkRoute('admin.manuals.create', 'Редактировать', [], ['class' => 'btn btn-md btn-default']) !!}
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if ($manual)
                {!! $manual->text !!}
            @endif
        </div>
    </div>

@stop
