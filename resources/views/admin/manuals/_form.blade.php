@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <hr>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH','route'=>['admin.manuals.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'route'=>['admin.manuals.index'], 'class' => 'form-horizontal']) !!}
@endif
<div class="form-group">
    {!! Form::label('text', 'Текст:', ['class' => 'control-label']) !!}
    {!! Form::textarea('text', null, ['class'=>'form-control ckEditor']) !!}
</div>

<div class="form-group">
    @if (isset($model) && $model->id == 1)
        {!! HTML::linkRoute('admin.manuals.index', 'Отмена', [], ['class' => 'btn btn-default']) !!}
    @endif
    {!! Form::submit(isset($model) ? 'Обновить' : 'Сохранить', ['class' => 'btn btn-default']) !!}
</div>

{!! Form::close() !!}

