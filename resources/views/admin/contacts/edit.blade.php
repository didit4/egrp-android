@extends('admin.layouts.default')

@section('content')

    <div class="row">

        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Редактировать {{$model->id == 1 ? 'контакты' : 'оферту'}}</h1>
            <hr>
        </div>

        <div class="col-md-8">
            @include('admin.contacts._form')
        </div>

    </div>

@stop
