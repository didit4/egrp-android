@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <hr>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH','route'=>['admin.questions.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'route'=>['admin.questions.index'], 'class' => 'form-horizontal']) !!}
@endif

<div class="form-group">
    <b>Имя:</b> {{$model->name}}
</div>

<div class="form-group">
    <b>Email:</b> {{$model->email}}
</div>

<div class="form-group">
    <b>Вопрос:</b><br/>
    {{$model->text}}
</div>

<div class="form-group">
    @if($model->answer)
        <b>Ответ:</b><br/>
        {{$model->answer}}
    @else
        {!! Form::label('answer', 'Ответ:', ['class' => 'control-label']) !!}
        {!! Form::textarea('answer', null, ['class'=>'form-control']) !!}
    @endif
</div>

<div class="form-group">
    {!! HTML::linkRoute('admin.questions.index', 'Назад', [], ['class' => 'btn btn-default']) !!}
    @if(!$model->answer)
        {!! Form::submit('Ответить', ['class' => 'btn btn-default send-answer']) !!}
    @endif
</div>

{!! Form::close() !!}

