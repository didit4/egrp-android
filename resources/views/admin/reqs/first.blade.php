@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Список заявок @parent @stop

{{-- Content --}}
@section('content')
    {!! Breadcrumbs::render('reqs_show_first', $model) !!}
    <div class="page-header without-top">
        <div class="row">
            <div class="col-sm-10">
                <a class="btn btn-default" href="{{ route('admin.reqs.index') }}"><- Вернуться в общий список заявок</a>
                <h3>Заявка # {{ $model->id }}</h3>
                <em class="bg-warning">Здесь отображена первоначальная версия заявки.</em>
            </div>
            <div class="col-sm-2">
                <div class="new-req-wrapper">
                    <div class="row">
                        <a href="{{ route('admin.reqs.show', [$model->id]) }}" class="btn btn-default">Текущая заявка</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-4">
            <b>{{$model->first_name}}&nbsp;{{$model->first_phone}}</b>
        </div>
        <div class="col-sm-8">
            {{$model->first_email}}
        </div>
    </div>
    <div class="row show-grid show-grid_gray">
        <div class="col-sm-2">
            <b>Cтатус</b>
        </div>
        <div class="col-sm-2 td-centered">
            <span class="label label-danger full-width">Новая</span>
            {{$model->updated_at}}<br>
            Оператор 1<br>
        </div>
        <div class="col-sm-2">
            <span class="label label-gray full-width">Обрабатывается</span>
        </div>
        <div class="col-sm-2">
            <span class="label label-light_gray full-width">В Росреестре</span>
        </div>
        <div class="col-sm-2">
            <span class="label label-light_gray full-width">Получен XML</span>
        </div>
        <div class="col-sm-2">
            <span class="label label-light_gray full-width">Отправлено на email</span>
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-2">
            <b>{{$model->getFirstNumName()}}</b>
        </div>
        <div class="col-sm-2">
            {{$model->getFirstNum()}}
        </div>
    </div>
    <hr/>
    <div class="row show-grid">
        <div class="col-sm-2">
            <b>Адрес</b>
        </div>
        <div class="col-sm-3">
            <b>Регион</b> <br>
            {{$model->first_region_name}}
        </div>
        <div class="col-sm-3">
            <b>Район</b> <br>
            {{$model->first_area_name}}
        </div>
        <div class="col-sm-3">
            <b>Населенный пункт</b> <br>
            {{$model->first_locality_name}}
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-3">
            <b>Тип улицы</b> <br>
            {{$model->street_type_name}}
        </div>
        <div class="col-sm-3">
            <b>Улица</b> <br>
            {{$model->first_street_name}}
        </div>
        <div class="col-sm-3">
            <b>Дом</b> <br>
            {{$model->first_house}}
        </div>
    </div>
    <div class="row show-grid">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-3">
            <b>Квартира</b> <br>
            {{$model->first_room}}
        </div>
        <div class="col-sm-3">
            <b>Строение</b> <br>
            {{$model->first_building}}
        </div>
        <div class="col-sm-3">
            <b>Корпус</b> <br>
            {{$model->first_housing}}
        </div>
    </div>
    <hr/>

@stop
