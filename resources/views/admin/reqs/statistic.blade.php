@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Список заявок @parent @stop

{{-- Content --}}
@section('content')
    {!! Breadcrumbs::render('reqs_statistic') !!}
    <div class="page-header without-top">
        <div class="row">
            <div class="col-sm-7">
                <h3>Статистика заявок</h3>
                <em class="bg-warning">Здесь отображены заявки по дням и количеству денег.</em>
            </div>
            <div class="col-sm-5">
                <div class="new-req-wrapper">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="stat-c">
                                Прибыль за
                                <select id="sel-months" class="">
                                    @foreach ($months as $key => $month)
                                        <option value="{{ $key }}" @if($currMonth == $key) selected="selected" @endif>{{$month}}</option>
                                    @endforeach
                                </select>: <span class="m-sum">{{ $sumCurrMonth }}</span> руб.
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="stat-c">
                            <a class="btn btn-default download" href="{{ route('admin.reqs.excel') }}">Выгрузить статистику</a>
                            <label><input class="all_stat" type="checkbox" value="1">Вся статистика</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table id="table" class="table table-striped table-hover table-verical">
        <thead>
            <tr>
                <th>Дата</th>
                <th>Кол-во новых</th>
                <th>Сумма, руб</th>
                <th>Сумма комиссии, руб</th>
                <th>Налоги 13%, руб</th>
                <th>Прибыль, руб</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($statistic as $item)
            <tr>
                <td>{{ $item->r_date }}</td>
                <td>{{ $item->r_count }}</td>
                <td>{{ $item->r_sum }}</td>
                <td>{{ $item->r_commission }}</td>
                <td>{{ $item->r_tax }}</td>
                <td>{{ $item->r_income }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
    <?php echo $statistic->render(); ?>
@stop
