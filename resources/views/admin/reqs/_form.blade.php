@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    <hr>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH','route'=>['admin.reqs.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'route'=>['admin.reqs.index'], 'class' => 'form-horizontal']) !!}
@endif
<div class="row show-grid show-grid_gray">
    <div class="col-sm-3">
        <b>Cтатус</b>
    </div>
    <div class="col-sm-2 td-centered">
        <span class="label {{ $colors[$model->status] }} full-width">{{$statuses[$model->status]}}</span>
        {{$model->updated_at}}
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-3">
    </div>
    <div class="col-sm-3">
        <input class="form-control" type="text" name="name" value="{{$model->name}}" />
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-3">
        <b>Контакты</b>
    </div>
    <div class="col-sm-3">
        <input class="form-control" type="text" name="phone" value="{{$model->phone}}" />
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-3">
    </div>
    <div class="col-sm-3">
        <input class="form-control" type="text" name="email" value="{{$model->email}}" />
    </div>
</div>
<hr/>
<div class="row show-grid">
    <div class="col-sm-3">
        <b>Кадастровый номер</b>
    </div>
    <div class="col-sm-3">
        <input class="form-control" type="text" name="cadastral_num" value="{{$model->cadastral_num}}" />
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-3">
        <b>Условный номер</b>
    </div>
    <div class="col-sm-3">
        <input class="form-control" type="text" name="conditional_num" value="{{$model->conditional_num}}" />
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-3">
        <b>Номер государственной регистрации права</b>
    </div>
    <div class="col-sm-3">
        <input class="form-control" type="text" name="srr_num" value="{{$model->srr_num}}" />
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-3">
        <b>Номер ограничения (обременения) права</b>
    </div>
    <div class="col-sm-3">
        <input class="form-control" type="text" name="limit_num" value="{{$model->limit_num}}" />
    </div>
</div>
<hr/>
<div class="row show-grid">
    <div class="col-sm-2">
        <b>Адрес</b>
    </div>
    <div class="col-sm-3">
        <b>Регион</b> <br>
        <select name="region_id" class="form-control">
            <option @if(!$model->region_id) selected @endif>Выберите регион</option>
            @if(isset($regions))
                @foreach($regions as $region)
                    <option value="{{ $region->place_id }}" @if($region->place_id == $model->region_id) selected @endif>{{ $region->name }}</option>
                @endforeach
            @endif
        </select>
        <input type="hidden" name="region_name" value="{{ $model->region_name  }}" />
    </div>
    <div class="col-sm-3">
        <b>Район</b> <br>
        <select name="area_id" class="form-control">
            <option @if(!$model->area_id) selected @endif>Выберите район</option>
            @if(isset($areas))
                @foreach($areas as $area)
                    <option value="{{ $area->place_id }}" @if($area->place_id == $model->area_id) selected @endif>{{ $area->name }}</option>
                @endforeach
            @endif
        </select>
        <input type="hidden" name="area_name" value="{{ $model->area_name  }}" />
    </div>
    <div class="col-sm-3">
        <b>Населенный пункт</b> <br>
        <select name="locality_id" class="form-control">
            <option @if(!$model->locality_id) selected @endif>Выберите населенный пункт </option>
            @if(isset($places))
                @foreach($places as $place)
                    <option value="{{ $place->place_id }}" @if($place->place_id == $model->locality_id) selected @endif>{{ $place->name }}</option>
                @endforeach
            @endif
        </select>
        <input type="hidden" name="locality_name" value="{{ $model->area_name  }}" />
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-3">
        <b>Тип улицы</b> <br>
        <select name="street_type" class="form-control">
            <option @if(!$model->street_type) selected @endif>Выберите тип улицы </option>
            @if(isset($sTypes))
                @foreach($sTypes as $sType)
                    <option value="{{ $sType->id }}" @if($sType->id == $model->street_type) selected @endif>{{ $sType->name }}</option>
                @endforeach
            @endif
        </select>
        <input type="hidden" name="street_type_name" value="{{ $model->street_type_name  }}" />
    </div>
    <div class="col-sm-3">
        <b>Улица</b> <br>
        <input class="form-control" type="text" name="street_name" value="{{$model->street_name}}" />
    </div>
    <div class="col-sm-3">
        <b>Дом</b> <br>
        <input class="form-control" type="text" name="house" value="{{$model->house}}" />
    </div>
</div>
<div class="row show-grid">
    <div class="col-sm-2">
    </div>
    <div class="col-sm-3">
        <b>Квартира</b> <br>
        <input class="form-control" type="text" name="room" value="{{$model->room}}" />
    </div>
    <div class="col-sm-3">
        <b>Строение</b> <br>
        <input class="form-control" type="text" name="building" value="{{$model->building}}" />
    </div>
    <div class="col-sm-3">
        <b>Корпус</b> <br>
        <input class="form-control" type="text" name="housing" value="{{$model->housing}}" />
    </div>
</div>
<hr/>
<button type="submit" class="btn btn-default">Сохранить</button>
<a class="btn btn-default" href="{{ route('admin.reqs.show', $model->id) }}">Отмена</a>
{!! Form::close() !!}

