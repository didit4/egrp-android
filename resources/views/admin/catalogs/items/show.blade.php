@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Бары @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>{{ $item->name }}</h3>
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.catalogs.items.create', 'Добавить', [], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Название</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($items as $item)
            <tr>
                <td>
                    <a href="{!!  URL::route('admin.catalogs.items.show', [$item->id]) !!}">
                        {{ $item->name }}
                    </a>
                </td>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.catalogs.items.edit', [$item->id]) !!}" class="btn btn-warning"
                           title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.catalogs.items.destroy', [$item->id]) !!}"
                           class="btn btn-danger deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
