@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH', 'files' => true, 'route'=>['admin.catalogs.items.update', $catalog, $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'files' => true, 'route'=>['admin.catalogs.items.index', $catalog], 'class' => 'form-horizontal']) !!}
@endif

<div class="form-group">
    {!! Form::label('name', 'Название:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descr', 'Описание:', ['class' => 'control-label']) !!}
    {!! Form::text('descr', null, ['class' => 'form-control']) !!}
</div>
@if (isset($model))
    <div class="form-group">
        <img class="admin-img" src="{{$model->img}}"/>
    </div>
@endif
<div class="form-group">
    {!! Form::label('img_tmp', 'Картинка:', ['class' => 'control-label']) !!}
    {!! Form::file('img_tmp', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
    {!! HTML::linkRoute('admin.catalogs.show', 'Отмена', [$catalog], array('class' => 'btn btn-default')) !!}
</div>

{!! Form::close() !!}

