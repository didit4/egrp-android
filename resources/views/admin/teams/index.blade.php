@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Команды @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Команды</h3>
        @if(!$teams->count())
            <em class="bg-warning">Ни одной команды еще не добавлено. Нажмите кнопку "Добавить", чтобы исправить это.</em>
        @endif
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.teams.create', 'Добавить', [], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Действия</th>
            <th>Логотип</th>
            <th>Название</th>
        </tr>
        </thead>
        <tbody id="sortable">
        @foreach ($teams as $team)
            <tr data-team_id="{{ $team->id }}">
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.teams.edit', [$team->id]) !!}" class="btn btn-warning"
                           title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.teams.destroy', [$team->id]) !!}"
                           class="btn btn-danger deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.teams.switch_active', [$team->id]) !!}"
                           class="btn {!! $team->active ? 'btn-primary' : 'btn-default' !!}" title="{!! $team->active ? 'Не показывать' : 'Активировать' !!}">
                            <span class="glyphicon {!! $team->active ? 'glyphicon-eye-open' : 'glyphicon-eye-close' !!}" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.teams.fav', [$team->id]) !!}"
                           class="btn {!! $team->fav ? 'btn-success' : 'btn-default' !!}" title="{!! $team->fav ? 'Обычная' : 'Выбор редакции' !!}">
                            <span class="glyphicon {!! $team->fav ? 'glyphicon-star' : 'glyphicon-star-empty' !!}" aria-hidden="true"></span>
                        </a>
                        <div class="btn btn-default sort-handler ui-sortable-handle">
                            <i class="glyphicon glyphicon-resize-vertical"></i>
                        </div>
                    </div>
                </td>
                <td><img height="40" src="{{ $team->getLogo() }}" /></td>
                <td>{{ $team->name }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
