@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>

@if (isset($model))
    {!! Form::model($model,['method' => 'PATCH', 'files' => true, 'route'=>['admin.teams.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'files' => true, 'route'=>['admin.teams.index'], 'class' => 'form-horizontal']) !!}
@endif

<div class="form-group">
    {!! Form::label('name', 'Название:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
@if (isset($model))
    <div class="form-group">
        <img class="admin-img" src="{{$model->logo}}"/>
    </div>
@endif
<div class="form-group">
    {!! Form::label('logo', 'Лого:', ['class' => 'control-label']) !!}
    {!! Form::file('logo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
    {!! HTML::linkRoute('admin.teams.index', 'Отмена', [], array('class' => 'btn btn-default')) !!}
</div>

{!! Form::close() !!}

