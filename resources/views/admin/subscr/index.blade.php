@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Подписки @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Подписки</h3>
        @if(!$subscrs->count())
            <em class="bg-warning">Никто не подписался =(</em>
        @endif
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Email</th>
            <th>Время подписки</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($subscrs as $subscr)
            <tr>
                <td>{{ $subscr->email }}</td>
                <td>{{ $subscr->created_at }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
