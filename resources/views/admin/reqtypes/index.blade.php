@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Типы заявки @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Типы заявок</h3>
        <em class="bg-warning">Здесь отображены типы заявок.</em>
    </div>
    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.reqtypes.create', 'Добавить', [], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>
    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="w10">Действия</th>
            <th>Название</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($types as $type)
            <tr>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.reqtypes.edit', [$type->id]) !!}" class="btn btn-default" title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.reqtypes.destroy', [$type->id]) !!}"
                           class="btn btn-default deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
                <td>{{ $type->name }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
