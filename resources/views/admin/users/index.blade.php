@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Пользователи @parent @stop

{{-- Content --}}
@section('content')

    <div class="page-header">
        <h3>Список пользователей</h3>
        <em class="bg-info">Здесь отображены пользователи, у которых есть доступ к управлению административной
            панелью»</em>
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.users.create', 'Добавить', null, ['class' => 'btn btn-md btn-default']) !!}
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Имя</th>
            <th>Email</th>
            <th>Роль</th>
            <th>Добавлен</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->getRoleName() }}</td>
                <td>{{ $user->created_at }}</td>
                <td>
                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{!!  URL::route('admin.users.edit', [$user->id]) !!}" class="btn btn-warning">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.users.destroy', [$user->id]) !!}"
                           class="btn btn-danger deleteObject">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop
