@extends('admin.layouts.default')

@section('content')
    <div class="row">

        <div class="col-lg-12">
            <p id="notice"></p>
            <h1>Добавить нового пользователя</h1>
            <hr>
        </div>

        <div class="col-md-8">
            @include('admin.users._form')
        </div>

    </div>
@stop