@extends('admin.layouts.default')

@section('content')

    <div class="page-header">
        <h3>Редактировать пользователя</h3>
    </div>

    <div class="col-md-8">
        @include('admin.users._form')
    </div>

@stop