<div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/admin"><?=Config::get('app.name');?></a>
    </div>
    <div id="navbar" class="navbar-default navbar-collapse collapse ">
        <ul class="nav navbar-nav">
            <li {{ strpos(Route::currentRouteName(), 'admin.reqs') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.reqs.index', 'Заявки') !!}</li>
            <li {{ strpos(Route::currentRouteName(), 'admin.faqs') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.faqs.index', 'Вопросы и ответы (FAQ)') !!}</li>
            <li {{ strpos(Route::currentRouteName(), 'admin.questions') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.questions.index', 'Вопросы от пользователей') !!}</li>
            @if(\Auth::getUser()->isAdmin())
                <li {{ strpos(Route::currentRouteName(), 'admin.contacts.index') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.contacts.index', 'Контакты') !!}</li>
                <li {{ strpos(Route::currentRouteName(), 'admin.contacts.offer') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.contacts.offer', 'Оферта') !!}</li>
                <li {{ strpos(Route::currentRouteName(), 'admin.paytypes') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.paytypes.index', 'Комиссия') !!}</li>
                <li {{ strpos(Route::currentRouteName(), 'admin.reqtypes') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.reqtypes.index', 'Типы заявок') !!}</li>
{{--                <li {{ strpos(Route::currentRouteName(), 'admin.manuals') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.manuals.index', 'Мануал') !!}</li>--}}
            @endif
        </ul>
        <ul class="nav navbar-nav navbar-right">
            @if(\Auth::getUser()->isAdmin())
                <li {{ strpos(Route::currentRouteName(), 'admin.users') !== false ? ' class=active' : null }}>{!! HTML::linkRoute('admin.users.index', 'Пользователи') !!}</li>
            @endif
            <li>{!! HTML::link('/logout', 'Выход') !!}</li>
        </ul>
    </div>
</div>
