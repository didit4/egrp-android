@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title') Игры @parent @stop

{{-- Content --}}
@section('content')
    <div class="page-header">
        <h3>Игры</h3>
        @if(!$events->count())
            <em class="bg-warning">Ни одной игры еще не добавлено. Нажмите кнопку "Добавить", чтобы исправить это.</em>
        @endif
    </div>

    <div class="row">
        <div class="col-xs-8">
            {!! HTML::linkRoute('admin.sport-events.create', 'Добавить', [], ['class' => 'btn btn-md btn-success']) !!}
        </div>
    </div>

    <table id="table" class="table table-striped table-hover">
        <thead>
        <tr>
            <th></th>
            <th>Команды</th>
            <th>Время начала</th>
            <th>Live</th>
            <th>Результат</th>
            <th>Бары</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($events as $event)
            <tr>
                <td>
                    <div class="btn-group btn-group-xs" role="group">
                        <a href="{!!  URL::route('admin.sport-events.edit', [$event->id]) !!}" class="btn btn-warning"
                           title="Редактировать">
                            <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.sport-events.destroy', [$event->id]) !!}"
                           class="btn btn-danger deleteObject" title="Удалить">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </a>
                        <a href="{!!  URL::route('admin.sport-events.switch_active', [$event->id]) !!}"
                           class="btn {!! $event->active ? 'btn-primary' : 'btn-default' !!}" title="{!! $event->active ? 'Не показывать' : 'Активировать' !!}">
                            <span class="glyphicon {!! $event->active ? 'glyphicon-eye-open' : 'glyphicon-eye-close' !!}" aria-hidden="true"></span>
                        </a>
                    </div>
                </td>
                <td>{{ $event->team1->name }} VS {{ $event->team2->name }}</td>
                <td>{{ date('d.m.Y H:i', $event->time) }}</td>
                <td>{{ $event->live }}</td>
                <td>{{ $event->result1 }} : {{ $event->result2 }}</td>
                <td><a href="{!!  URL::route('admin.bars.event', [$event->id]) !!}" class="btn btn-default btn-xs"
                       title="Список баров">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                        {{ $event->bars()->count() }}
                    </a>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
@stop
