@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<hr>
<? $time = null; ?>
@if (isset($model))
    <? $time = date('d.m.Y H:i', $model->time);?>
    {!! Form::model($model,['method' => 'PATCH', 'files' => true, 'route'=>['admin.sport-events.update', $model->id], 'class' => 'form-horizontal']) !!}
@else
    {!! Form::open(['method' => 'POST', 'files' => true, 'route'=>['admin.sport-events.index'], 'class' => 'form-horizontal']) !!}
@endif
<div class="form-group">
    {!! Form::label('tournament_id', 'Турнир:', ['class' => 'control-label']) !!}
    {!! Form::select('tournament_id', $tournaments, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('team1_id', 'Команда 1:', ['class' => 'control-label']) !!}
    {!! Form::select('team1_id', $teams, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('team2_id', 'Команда 2:', ['class' => 'control-label']) !!}
    {!! Form::select('team2_id', $teams, null, ['class' => 'form-control']) !!}
</div>
@if (isset($model))
    <div class="form-group">
        {!! Form::label('time', 'Время начала:', ['class' => 'control-label']) !!}
        {!! Form::text('time', $time, ['class' => 'form-control', 'id' => 'datepicker']) !!}
    </div>
@else
    <div class="form-group">
        {!! Form::label('time', 'Время начала:', ['class' => 'control-label']) !!}
        {!! Form::text('time', null, ['class' => 'form-control', 'id' => 'datepicker']) !!}
    </div>
@endif
<div class="form-group">
    {!! Form::label('live', 'Live:', ['class' => 'control-label']) !!}
    {!! Form::checkbox('live', 1, null) !!}
</div>
<div class="form-group">
    {!! Form::label('result1', 'Результат игры:', ['class' => 'control-label']) !!}
    <div class="form-group row">
        <div class="col-xs-4 col-sm-2">
            {!! Form::text('result1', null, ['class' => 'form-control']) !!}
        </div>
        <label class="col-xs-2 col-sm-1 control-label">:</label>
        <div class="col-xs-4 col-sm-2">
            {!! Form::text('result2', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    {!! Form::submit(isset($model) ? 'Обновить' : 'Создать', ['class' => 'btn btn-default']) !!}
    {!! HTML::linkRoute('admin.sport-events.index', 'Отмена', [], array('class' => 'btn btn-default')) !!}
</div>

{!! Form::close() !!}

