<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>ЕГРН сегодня</title>
	<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i,700,700i&subset=cyrillic"
		  rel="stylesheet">

	<!-- Styles -->
	<link rel="stylesheet" href="/main/css/main.css">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>
	<![endif]-->
</head>
<body class="">


<header class="header">
	<div class="header__container">
		<a href="/" class="header__logo">
			<img src="/main/img/logo.png" alt=""/>
		</a>
	</div>
</header>
<div id="content" class="fd-wrapper">
	<!--<div class="fd-wrapper__table">-->
	<div class="fd-wrapper__table-cell">
		<main class="fd-wrapper__content">
			<div class="fd-wrapper__container">
				<!--blocks-->
				<div class="fd-text">
					<h2>Заказать сведения ЕГРН!</h2>
					<p>
						Информация, скорость, результат
					</p>

					<div class="fd-text__link-wrapper">
						<div class="fd-text__link-item">
							<a href="https://appsto.re/ru/OTQijb.i" class="fd-text__link"><img src="/main/img/app-store.png" alt=""></a>
						</div>
						<div class="fd-text__link-item">
							<a href="https://play.google.com/store/apps/details?id=com.mediapark.egrptoday" class="fd-text__link"><img src="/main/img/google-play.png" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
	<!--</div>-->

</div>
<footer class="footer">
	<div class="footer__container">
		<div class="footer__row">
			<div class="footer__col">
				<div class="footer__copy">©2017 Все права защищены.</div>
			</div>
			<div class="footer__col footer__col_right">
				<nav class="footer-menu">
					<ul class="footer-menu__list">
						<li class="footer-menu__list-item">
							<a href="mailto:{{$contact->email}}" class="footer-menu__list-link">Напишите нам</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/faq" class="footer-menu__list-link">Вопросы и ответы</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/offer" class="footer-menu__list-link">Оферта</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>

<!-- Scripts -->
<script src="/main/js/vendor.js"></script>
<script src="/main/js/main.js"></script>
</body>
</html>
