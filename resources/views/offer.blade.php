<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<title>ЕГРН сегодня</title>
	<link rel="apple-touch-icon" href="./img/apple-touch-icon.png">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i,700,700i&subset=cyrillic"
		  rel="stylesheet">

	<!-- Styles -->
	<link rel="stylesheet" href="/main/css/main.css">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.0.8/es5-shim.min.js"></script>
	<![endif]-->
	<meta name="yandex-verification" content="a015689f210da137" />
</head>
<body class="">


<header class="header">
	<div class="header__container">
		<a href="/" class="header__logo">
			<img src="/main/img/logo.png" alt=""/>
		</a>
	</div>
</header>
<div id="content" class="fd-wrapper">
	<!--<div class="fd-wrapper__table">-->
	<div class="fd-wrapper__table-cell">
		<main class="fd-wrapper__content">
			<div class="fd-wrapper__container">
				<!--blocks-->
				<div class="fd-text fd-text_offer">
					{!! $contact->text !!}
				</div>
			</div>
		</main>
	</div>
	<!--</div>-->

</div>
<footer class="footer">
	<div class="footer__container">
		<div class="footer__row">
			<div class="footer__col">
				<div class="footer__copy">©2017 Все права защищены.</div>
			</div>
			<div class="footer__col footer__col_right">
				<nav class="footer-menu">
					<ul class="footer-menu__list">
						<li class="footer-menu__list-item">
							<a href="mailto:{{$email->email}}" class="footer-menu__list-link">Напишите нам</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/faq" class="footer-menu__list-link">Вопросы и ответы</a>
						</li>
						<li class="footer-menu__list-item">
							<a href="/offer" class="footer-menu__list-link">Оферта</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>

<!-- Scripts -->
<script src="/main/js/vendor.js"></script>
<script src="/main/js/main.js"></script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter45125202 = new Ya.Metrika({
					id:45125202,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true,
					webvisor:true,
					trackHash:true
				});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = "https://mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45125202" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
