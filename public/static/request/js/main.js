window.orderData={price:400,payType:1,web:1},$(document).ready(function(){function e(){$.ajax({type:"GET",data:{region_id:window.orderData.region_id},url:"https://egrp.today/api/v1/req/get_areas",success:function(e){if(e.areas&&e.areas.length){var t='<option value="">Район</option>';e.areas.map(function(e){t+='<option value="'+e.id+'">'+e.name+"</option>"}),$("#select-2").empty().html(t).prop("disabled",!1).material_select()}else $("#select-2").empty().html('<option value="">Район</option>').prop("disabled",!0).material_select()}})}function t(){$.ajax({type:"GET",data:{area_id:window.orderData.area_id},url:"https://egrp.today/api/v1/req/get_places",success:function(e){if(e.places&&e.places.length){var t='<option value="">Населенный пункт</option>';e.places.map(function(e){t+='<option value="'+e.id+'">'+e.name+"</option>"}),$("#select-3").empty().html(t).prop("disabled",!1).material_select()}else $("#select-3").empty().html('<option value="">Населенный пункт</option>').prop("disabled",!0).material_select()}})}function n(e){return e=e.replace(/ /g,""),e=e.replace(/\(/g,""),e=e.replace(/\)/g,""),e=e.replace(/-/g,"")}function a(e){return e=e.replace(/\+/g,"")}$(".fd-tab-container").length>0&&($(".fd-tab-container").easytabs({animate:!0,animationSpeed:"fast",updateHash:!1}),$(".fd-tab-container").bind("easytabs:before",function(e,t,n){return!t.is(".disabled")})),$("select").material_select(),$.ajax({type:"GET",url:"https://egrp.today/api/v1/req/get_regions",success:function(e){if(e.regions){var t='<option value="">Регион</option>';e.regions.map(function(e){t+='<option value="'+e.id+'">'+e.name+"</option>"}),$("#select-1").empty().html(t).material_select()}}}),$.ajax({type:"GET",url:"https://egrp.today/api/v1/req/get_type_st",success:function(e){if(e.st_types){var t='<option value="">Тип улицы</option>';e.st_types.map(function(e){t+='<option value="'+e.id+'">'+e.name+"</option>"}),$("#select-4").empty().html(t).material_select()}}}),$.ajax({type:"GET",url:"https://egrp.today/api/v1/req/get_price",success:function(e){e.price&&(window.orderData.price=parseInt(e.price))}}),$.ajax({type:"GET",url:"https://egrp.today/api/v1/req/get_types",success:function(e){if(e.types){var t="";e.types.map(function(e,n){t+='<div class="input-field"><input name="group1" type="radio" id="radio'+n+'" value="'+e.id+'"'+(0==n?'checked="checked"':"")+'/><label for="radio'+n+'">'+e.name+"</label></div>"}),$(".types").html(t)}else $(".first-step").hide(),$(".main-form").fadeIn()}}),$(".goto-start").on("click",function(e){e.preventDefault(),$(".first-step").hide(),$(".main-form").fadeIn(),window.orderData.type=parseInt($("input[name=group1]:checked").val()),console.log(window.orderData)}),$("#select-1").on("change",function(t){t.preventDefault(),window.orderData.region_id=parseInt($(this).val()),e()}),$("#select-2").on("change",function(e){e.preventDefault(),window.orderData.area_id=parseInt($(this).val()),t()}),$("#select-3").on("change",function(e){e.preventDefault(),window.orderData.locality_id=parseInt($(this).val())}),$("#select-4").on("change",function(e){e.preventDefault(),window.orderData.street_type=parseInt($(this).val())}),$("#form1 input[type=text]").on("keyup blur",function(){var e=$(this).data("name");e&&(window.orderData[e]=$(this).val())}),$.validator.addMethod("input1",function(e,t,n){return(""!==e||""!==$("#input-2").val()&&""!==$("#input-3").val()&&""!==$("#select-1").val())&&!(e.replace(/:/g,"").length<13)}),$.validator.addMethod("input2",function(e,t,n){return(!(""===$("#input-1").val()||$("#input-1").val().replace(/:/g,"").length<13)||""!==e)&&(""!==$("#select-1").val()&&""!==$("#select-4").val())});var r=$("#form1").validate({rules:{"input-1":{input1:!0},input2:{input2:!0},input3:{input2:!0,number:!0}},errorElement:"span",messages:{"input-1":"Неверный формат",input2:"Неверный формат",input3:"Неверный формат"},submitHandler:function(e){},errorPlacement:function(e,t){}});$("#input-1").mask("99:99:9999999:99000",{placeholder:"00:00:0000000:00000"}),$("#input-9").mask("+7 (999) 999-99-99",{placeholder:"+7 (. . .) . . . - . . - . ."}),$(".goto2").on("click",function(e){e.preventDefault(),$(".error-message").remove();var t=!0;if(""===$("#input-1").val()||$("#input-1").val().replace(/:/g,"").length<13){if(""===$("#select-1").val()){var n='<div class="error-message">Выберите регион</div>';$("#select-1").parent(".select-wrapper").append(n).find(".select-dropdown").addClass("error"),t=!1}else $("#select-1").parent(".select-wrapper").find(".error-message").remove(),$("#select-1").parent(".select-wrapper").find(".select-dropdown").removeClass("error");if(""===$("#select-4").val()){var n='<div class="error-message">Выберите тип улицы</div>';$("#select-4").parent(".select-wrapper").append(n).find(".select-dropdown").addClass("error"),t=!1}else $("#select-4").parent(".select-wrapper").find(".error-message").remove(),$("#select-4").parent(".select-wrapper").find(".select-dropdown").removeClass("error")}$("#form1").submit(),r.errorList.map(function(e){var t='<div class="error-message">'+e.message+"</div>";$(e.element).parent().append(t)}),t&&(t=$("#form1").valid()),t&&($(".step-2").removeClass("disabled").click(),window.orderData.cadastral_num=$("#input-1").val())});var i=$("#form2").validate({rules:{"input-7":{required:!0},"input-8":{required:!0,email:!0},"input-9":{required:!0}},errorElement:"span",messages:{"input-7":"Неверный формат","input-8":"Неверный формат","input-9":"Неверный формат"},submitHandler:function(e){},errorPlacement:function(e,t){}});$(".goto3").on("click",function(e){e.preventDefault(),$("#form2").submit(),$(".error-message").remove(),i.errorList.map(function(e){var t='<div class="error-message">'+e.message+"</div>";$(e.element).parent().append(t)}),$("#form2").valid()&&(window.orderData.name=$("#input-7").val(),window.orderData.email=$("#input-8").val(),window.orderData.phone=n($("#input-9").val()),$("#form-ac input[name=sum]").val(window.orderData.price),$("#form-pc input[name=sum]").val(window.orderData.price),$(".sum_txt").html(window.orderData.price),$("#form-ac input[name=cps_email]").val(window.orderData.email),$("#form-pc input[name=cps_email]").val(window.orderData.email),$("#form-ac input[name=cps_phone]").val(a(window.orderData.phone)),$("#form-pc input[name=cps_phone]").val(a(window.orderData.phone)),$("#form-ac input[name=custName]").val(window.orderData.name),$("#form-pc input[name=custName]").val(window.orderData.name),$(".step-3").removeClass("disabled").click())});var o=!1;$(".create-order-btn").on("click",function(e){e.preventDefault(),o||(o=!0,$.ajax({type:"POST",url:"https://egrp.today/api/v1/req/new_request",data:window.orderData,success:function(e){e.req&&($("#form-ac input[name=customerNumber]").val("customer_"+e.req),$("#form-ac input[name=orderNumber]").val("order_"+e.req),$("#form-pc input[name=customerNumber]").val("customer_"+e.req),$("#form-pc input[name=orderNumber]").val("order_"+e.req),$("#form-ac").submit()),o=!1}}))});var s=$("#form3").validate({rules:{"input-status-1":{required:!0},"input-9":{required:!0}},errorElement:"span",messages:{"input-status-1":"Неверный формат","input-9":"Неверный формат"},submitHandler:function(e){},errorPlacement:function(e,t){}});$(".get-status").on("click",function(e){e.preventDefault(),$("#form3").submit(),$(".error-message").remove(),s.errorList.map(function(e){var t='<div class="error-message">'+e.message+"</div>";$(e.element).parent().append(t)}),$("#form3").valid()&&($("#form3").hide(),$.ajax({type:"GET",url:"https://egrp.today/api/v1/req/get",data:{phone:n($("#input-9").val()),number:$("#input-status-1").val()},success:function(e){if(console.log("response",e),e.req){$(".fd-etabs__title-3").html("ДАННЫЕ по заявке №"+$("#input-status-1").val()+" // "+$("#input-9").val());var t=e.req.status;if(t){for(var n=0;n<t;n++)$(".fd-status__list .fd-status__list-item").eq(n).addClass("gone");$(".fd-status__list .fd-status__list-item").eq(t).addClass("active")}e.req.created&&$(".fd-status__list .fd-status__list-item").eq(0).find(".fd-status__date").html(e.req.created),e.req.processed_at&&$(".fd-status__list .fd-status__list-item").eq(1).find(".fd-status__date").html(e.req.processed_at),e.req.ros_at&&$(".fd-status__list .fd-status__list-item").eq(2).find(".fd-status__date").html(e.req.ros_at),e.req.xml_at&&$(".fd-status__list .fd-status__list-item").eq(3).find(".fd-status__date").html(e.req.xml_at),e.req.sent_at&&$(".fd-status__list .fd-status__list-item").eq(4).find(".fd-status__date").html(e.req.sent_at),e.req.not_at&&($(".fd-status__list .fd-status__list-item").eq(4).removeClass("gone").addClass("active").find(".fd-status__date").html(e.req.not_at),$(".fd-status__list .fd-status__list-item").eq(4).find(".fd-status__body span").html("Нет в Росреестре"),$(".fd-status__list .fd-status__list-item").eq(3).remove()),$(".result-request").fadeIn()}else $(".fd-etabs__wrapper").append(e.error)},error:function(e){var t=$.parseJSON(e.responseText);$(".fd-etabs__wrapper").append(t.error)}}))})}),function(e){"use strict";function t(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e}function n(e,n){this.el=e,this.options=t({},this.options),t(this.options,n),this._init()}var a={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",msTransition:"MSTransitionEnd",transition:"transitionend"},r=a[Modernizr.prefixed("transition")],i={transitions:Modernizr.csstransitions};n.prototype.options={closeEl:"",onBeforeOpen:function(){return!1},onAfterOpen:function(){return!1},onBeforeClose:function(){return!1},onAfterClose:function(){return!1}},n.prototype._init=function(){this.button=this.el.querySelector("button"),this.expanded=!1,this.contentEl=this.el.querySelector(".morph-content"),this._initEvents()},n.prototype._initEvents=function(){var e=this;if(this.button.addEventListener("click",function(){e.toggle()}),""!==this.options.closeEl){var t=this.el.querySelector(this.options.closeEl);t&&t.addEventListener("click",function(){e.toggle()})}},n.prototype.toggle=function(){if(this.isAnimating)return!1;this.expanded?this.options.onBeforeClose():(classie.addClass(this.el,"active"),this.options.onBeforeOpen()),this.isAnimating=!0;var e=this,t=function(n){if(n.target!==this)return!1;if(i.transitions){if(e.expanded&&"opacity"!==n.propertyName||!e.expanded&&"width"!==n.propertyName&&"height"!==n.propertyName&&"left"!==n.propertyName&&"top"!==n.propertyName)return!1;this.removeEventListener(r,t)}e.isAnimating=!1,e.expanded?(classie.removeClass(e.el,"active"),e.options.onAfterClose()):e.options.onAfterOpen(),e.expanded=!e.expanded};i.transitions?this.contentEl.addEventListener(r,t):t();var n=this.button.getBoundingClientRect();classie.addClass(this.contentEl,"no-transition"),this.contentEl.style.left="auto",this.contentEl.style.top="auto",setTimeout(function(){e.contentEl.style.left=n.left+"px",e.contentEl.style.top=n.top+"px",e.expanded?(classie.removeClass(e.contentEl,"no-transition"),classie.removeClass(e.el,"open")):setTimeout(function(){classie.removeClass(e.contentEl,"no-transition"),classie.addClass(e.el,"open")},25)},25)},e.UIMorphingButton=n}(window),function(){function e(){window.scrollTo(s?s.x:0,s?s.y:0)}function t(){window.removeEventListener("scroll",r),window.addEventListener("scroll",e)}function n(){window.addEventListener("scroll",r)}function a(){window.removeEventListener("scroll",e),n()}function r(){o||(o=!0,setTimeout(function(){i()},60))}function i(){s={x:window.pageXOffset||l.scrollLeft,y:window.pageYOffset||l.scrollTop},o=!1}var o,s,l=window.document.documentElement;n(),[].slice.call(document.querySelectorAll(".morph-button")).forEach(function(e){new UIMorphingButton(e,{closeEl:".icon-close",onBeforeOpen:function(){t()},onAfterOpen:function(){a()},onBeforeClose:function(){t()},onAfterClose:function(){a()}})}),[].slice.call(document.querySelectorAll("form button")).forEach(function(e){e.addEventListener("click",function(e){e.preventDefault()})})}();