/**
 * Custom scripts
 */




/**
 * Custom scripts
 */

$(window).load(function () {

	if ($('input[type=tel]').length > 0) {
		$('input[type=tel]').mask('+7 (999) 999-99-99', {placeholder:'+7 (. . .) . . . - . . - . .' });
	}
	if ($('input[type=tel]').length > 0) {
		$('input#text2').mask('000 000 000 000', {reverse: true});
	}
});
/**
 * Created by Anton on 04.10.2016.
 */

$(document).ready(function () {
	$('#tab-container').easytabs({
		panelContext: $('.panel-container')
	});
	var $tabContainer = $('#tab-container'),
		$tabs = $tabContainer.data('easytabs').tabs;
	$('.next-tab').click(function() {
		var i = parseInt($(this).attr('rel'));
		var tabSelector = $tabs.children('a:eq(' + i + ')').attr('href');
		$tabContainer.easytabs('select', tabSelector);
		return false;
	});

	// Materialize.updateTextFields();
	$('select').material_select();

	/*var slider = document.getElementsByClassName('fd-slider');
	noUiSlider.create(slider, {
		start: [20, 80],
		connect: true,
		step: 1,
		range: {
			'min': 0,
			'max': 100
		},
		format: wNumb({
			decimals: 0
		})
	});*/


	var slider2 = document.getElementById("slider"),
		leftValue = document.getElementById('leftvalue'),
		rightValue = document.getElementById('rightvalue');

// 0 = initial minutes from start of day
// 1440 = maximum minutes in a day
// step: 30 = amount of minutes to step by.
	var initialStartMinute = 540,
		initialEndMinute = 1680,
		step = 30;

	var slider = noUiSlider.create(slider2,{
		start:[initialStartMinute + 180,initialEndMinute - 180],
		connect:true,
		step:step,
		range:{
			'min':initialStartMinute,
			'max':initialEndMinute
		}
	});

	var convertValuesToTime = function(values,handle){
		var hours = 0,
			minutes = 0;

		if(handle === 0){
			hours = convertToHour(values[0]);
			minutes = convertToMinute(values[0],hours);
			leftValue.innerHTML = formatHoursAndMinutes(hours,minutes);
			return;
		};

		hours = convertToHour(values[1]);
		minutes = convertToMinute(values[1],hours);
		rightValue.innerHTML = formatHoursAndMinutes(hours,minutes);

	};

	var convertToHour = function(value){
		if(value>=1440){
			return Math.floor((value - 1440) / 60);

		} else {
			return Math.floor(value / 60);
		}

	};
	var convertToMinute = function(value,hour){
		if(value>=1440){
			return value - 1440 - hour * 60;

		} else {
			return value - hour * 60;
		}

	};
	var formatHoursAndMinutes = function(hours,minutes){
		if(hours.toString().length == 1) hours = '0' + hours;
		if(minutes.toString().length == 1) minutes = '0' + minutes;
		return hours+':'+minutes;
	};

	slider.on('update',function(values,handle){
		convertValuesToTime(values,handle);
	});
});