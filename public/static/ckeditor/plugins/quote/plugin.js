CKEDITOR.plugins.add('quote', {
	icons: 'quote',
	init: function(editor) {
		editor.addCommand('quote', new CKEDITOR.dialogCommand('quoteDialog'));

		editor.ui.addButton('Quote', {
			label: 'Вставить цитату',
			command: 'quote'
		});

		CKEDITOR.dialog.add('quoteDialog', this.path + 'dialogs/quote.js');
	}
});