$(".deleteObject").on('click', function (e) {
    e.preventDefault();

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var url = $(this).attr('href');

    if (confirm('Вы дейстительно хотите удалить?')) {
        $.ajax({
            type: 'DELETE',
            url: url, //resource
            success: function (response) {
                window.location.reload();
            }
        });
    }
});

$(document).ready(function () {

    // Set active link in navbar
    $("#navbar ul li").each(function () {
        var navLink = $(this).find('a').attr('href');
        if (typeof navLink != 'undefined' && navLink == window.location.href.slice(0, navLink.length)) {
            $(this).addClass('active');
            return false;
        }
    });

});
