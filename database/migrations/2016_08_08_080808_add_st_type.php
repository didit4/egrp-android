<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reqs', function($table)
		{
			$table->integer('street_type');
			$table->string('street_type_name');
			$table->integer('first_street_type');
			$table->string('first_street_type_name');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reqs', function($table)
		{
			$table->dropColumn('street_type');
			$table->dropColumn('street_type_name');
			$table->dropColumn('first_street_type');
			$table->dropColumn('first_street_type_name');
		});
	}

}
