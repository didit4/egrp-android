<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReqs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reqs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('status');
			$table->string('cadastral_num');
			$table->string('conditional_num');
			$table->string('srr_num');
			$table->string('limit_num');
			$table->string('name');
			$table->string('phone');
			$table->string('email');
			$table->integer('operator_id');
			$table->integer('region_id');
			$table->string('region_name');
			$table->integer('area_id');
			$table->string('area_name');
			$table->integer('locality_id');
			$table->string('locality_name');
			$table->integer('street_id');
			$table->string('street_name');
			$table->integer('house');
			$table->integer('price');
//первоначальная версия
            $table->string('first_name');
            $table->string('first_phone');
            $table->string('first_email');
            $table->integer('first_region_id');
            $table->string('first_region_name');
            $table->integer('first_area_id');
            $table->string('first_area_name');
            $table->integer('first_locality_id');
            $table->string('first_locality_name');
            $table->integer('first_street_id');
            $table->string('first_street_name');
            $table->integer('first_house');
            $table->string('first_cadastral_num');
            $table->string('first_conditional_num');
            $table->string('first_srr_num');
            $table->string('first_limit_num');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reqs');
	}

}
