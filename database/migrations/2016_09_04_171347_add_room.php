<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoom extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reqs', function($table)
		{
			$table->string('room');
			$table->string('building');
			$table->string('housing');
			$table->string('first_room');
			$table->string('first_building');
			$table->string('first_housing');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reqs', function($table)
		{
			$table->dropColumn('room');
			$table->dropColumn('building');
			$table->dropColumn('housing');
			$table->dropColumn('first_room');
			$table->dropColumn('first_building');
			$table->dropColumn('first_housing');
		});
	}

}
