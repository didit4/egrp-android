<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reqs', function($table)
		{
			$table->timestamp('processed_at');
			$table->timestamp('ros_at');
			$table->timestamp('xml_at');
			$table->timestamp('sent_at');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reqs', function($table)
		{
			$table->dropColumn('processed_at');
			$table->dropColumn('ros_at');
			$table->dropColumn('xml_at');
			$table->dropColumn('sent_at');
		});
	}

}
